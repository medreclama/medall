import header from '../../blocks/header/header';
import headerNavigation from '../../blocks/header-navigation/header-navigation';
import headerSearch from '../../blocks/header-search/header-search';
import lb from '../../common/scripts/modules/lightbox';
import modal from '../../blocks/modal/modal';
import Validator from '../../common/scripts/modules/form/Validator';
import { footerGalleryInit } from '../../blocks/footer-gallery/footer-gallery';
import { initFields } from '../../blocks/form-input/form-input';
import Form from '../../common/scripts/modules/form/Form';
import counters from '../../common/scripts/modules/counters';
import { iMaskInit } from '../../blocks/form/form';
import media from '../../common/scripts/modules/match-media';
import Slider from '../../blocks/slider/slider';
import anchorScroll from '../../blocks/anchor-scroll/anchor-scroll';
import { accessibilityPanel } from '../../blocks/accessibility-panel/accessibility-panel';
import { accessibilityMode } from '../../blocks/accessibility-mode/accessibility-mode';
import { headerServices, headerServicesOthers } from '../../blocks/header-services/header-services';
import youtubeLoadingLazy from '../../blocks/iframe-responsive/iframe-responsive';

window.Slider = Slider;
document.addEventListener('DOMContentLoaded', () => {
  window.media = media;
  media.init();
  footerGalleryInit();
  header.init();
  headerNavigation.init();
  headerSearch.init();
  initFields();
  lb();
  modal();
  window.Validator = Validator;
  document.querySelectorAll('form.form-ajax').forEach((form) => {
    const formInit = new Form(form);
    formInit.init();
    const formInputs = document.querySelectorAll('.form-input');
    form.addEventListener('submit', () => {
      setTimeout(() => {
        formInputs.forEach((formInput) => {
          if (formInput.classList.contains('form-input--has-value')) {
            formInput.classList.remove('form-input--has-value');
            formInput.querySelector('.form-input__label').classList.remove('form-input__label--hidden');
          }
        });
      }, 300);
    });
  });
  iMaskInit();
  anchorScroll();
  counters(`
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();
    for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
    k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(67423126, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
    });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/67423126" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
  `, 'head');
  if (window.location.href !== 'https://medall.clinic/dentistry/') {
    counters(`
      <!-- comfortel -->
      <script type="text/javascript">
          var _calltr_obj=".phone";
      </script>
      <script src="https://virt117.pbx.comfortel.pro/call-tracking.js?_id=1624540366" type="text/javascript" async></script>
      <!-- comfortel -->
  `, 'head');
  }
  const countersCodeBody = `
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
  `;
  counters(countersCodeBody);
  document.querySelectorAll('.form-ajax')?.forEach((form) => {
    form.addEventListener('submit', () => ym(67423126, 'reachGoal', 'zayvka')); // eslint-disable-line no-undef
  });
  document.getElementById('B24_FORM_2_END')?.addEventListener('submit', () => {
    ym(67423126, 'reachGoal', 'B24_FORM_2_END');// eslint-disable-line no-undef
  });
  accessibilityPanel.init();
  accessibilityMode.init();
  headerServices();
  headerServicesOthers();
  youtubeLoadingLazy();
});
