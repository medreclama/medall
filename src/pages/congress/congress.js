import { congressSliderInteresting } from '../../blocks/congress/slider-interesting/slider-interesting';
import { congressSliderSpeakers } from '../../blocks/congress/slider-speakers/slider-speakers';
import { sliderProgram } from '../../blocks/congress/slider-program/slider-program';

document.addEventListener('DOMContentLoaded', () => {
  congressSliderInteresting();
  congressSliderSpeakers();
  sliderProgram();
});
