import { ReviewsPlatforms } from '../../blocks/reviews/reviews-platforms/reviews-platforms';
import { ReviewsFilter } from '../../blocks/reviews/reviews-filter/reviews-filter';
import { reviewsItem } from '../../blocks/reviews/reviews-item/reviews-item';

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new ReviewsPlatforms();
  const reviewsFilter = new ReviewsFilter('.reviews-filter');
  reviewsFilter.init();
  reviewsItem();
});
