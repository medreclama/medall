import { HomeImportant } from '../../blocks/home-important/home-important';
import { homeStaffInit } from '../../blocks/home-staff/home-staff';
import { HomeReasons } from '../../blocks/home-reasons/home-reasons';
import { HomeFeedback } from '../../blocks/home-feedback/home-feedback';
import { SliderWorld } from '../../blocks/slider-world/slider-world';
import { HomeDirections } from '../../blocks/home-directions/home-directions';
import { HomeCategoryDirections } from '../../blocks/home-category-directions/home-category-directions';
import { categoryDirectionInit } from '../../blocks/category-direction/category-direction';
import { reviewsItem } from '../../blocks/reviews/reviews-item/reviews-item';

document.addEventListener('DOMContentLoaded', () => {
  // eslint-disable-next-line no-new
  new HomeImportant();
  homeStaffInit();
  // eslint-disable-next-line no-new
  new HomeDirections();
  // eslint-disable-next-line no-new
  new HomeReasons();
  // eslint-disable-next-line no-new
  new HomeFeedback();
  // eslint-disable-next-line no-new
  new SliderWorld();
  // eslint-disable-next-line no-new
  new HomeCategoryDirections();
  categoryDirectionInit();
  reviewsItem();
});
