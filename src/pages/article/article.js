import { articlesLayout } from '../../blocks/articles-layout/articles-layout';

document.addEventListener('DOMContentLoaded', () => {
  articlesLayout.init();
});
