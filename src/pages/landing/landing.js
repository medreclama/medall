import { landingBlock, landingBlockSEOInit } from '../../blocks/landing-block/landing-block';
import { LandingStories } from '../../blocks/landing-stories/landing-stories';
import landingFacts from '../../blocks/landing-facts/landing-facts';
import { LandingWorks } from '../../blocks/landing-works/landing-works';
import landingReason from '../../blocks/landing-reason/landing-reason';
import landingAdvantages from '../../blocks/landing-advantages/landing-advantages';
import question from '../../blocks/question/question';
import landingTeam from '../../blocks/landing-team/landing-team';
import sliderIndications from '../../blocks/slider-indications/slider-indications';
import { LandingComfort } from '../../blocks/landing-comfort/landing-comfort';
import fontResizer from '../../common/scripts/modules/font-resizer';
import { homeStaffInit } from '../../blocks/home-staff/home-staff';
import { HomeReasons } from '../../blocks/home-reasons/home-reasons';
import { HomeImportant } from '../../blocks/home-important/home-important';
import { CategoryDirections } from '../../blocks/category-directions/category-directions';
import { categoryDirectionInit } from '../../blocks/category-direction/category-direction';
import ImageCompareClass from '../../common/scripts/modules/image-compare';
import { HomeFeedback } from '../../blocks/home-feedback/home-feedback';
import { reviewsItem } from '../../blocks/reviews/reviews-item/reviews-item';
import { tablePriceInit } from '../../blocks/table-price/table-price';
import { questionsList, questionsSources } from '../../blocks/questions-list/questions-list';
import { LandingBlockSectionH3 } from '../../blocks/landing-block-section-h3/landing-block-section-h3';
import { LandingBannerSlider } from '../../blocks/landing-banner-slider/landing-banner-slider';
import { LandingVideo } from '../../blocks/landing-video/landing-video';
import { addTypograf } from '../../common/scripts/modules/typograf';

document.addEventListener('DOMContentLoaded', () => {
  landingAdvantages();
  landingBlock();
  landingFacts();
  landingReason();
  landingTeam();
  homeStaffInit();
  // homeReasonsInit();
  question();
  sliderIndications();
  fontResizer.init('.landing-price__name, .landing-header__title');
  // eslint-disable-next-line no-new
  new LandingComfort();
  // eslint-disable-next-line no-new
  new LandingWorks();
  // eslint-disable-next-line no-new
  new LandingStories();
  // eslint-disable-next-line no-new
  new HomeReasons();
  // eslint-disable-next-line no-new
  new HomeImportant();
  // eslint-disable-next-line no-new
  new CategoryDirections();
  // eslint-disable-next-line no-new
  new HomeFeedback();
  categoryDirectionInit();
  const imageCompareOptions = {
    controlColor: '#F5F8F6',
    startingPoint: 10,
  };
  const worksImageCompare = new ImageCompareClass(imageCompareOptions);
  worksImageCompare.init();
  landingBlockSEOInit();
  addTypograf();
  reviewsItem();
  tablePriceInit();
  questionsList();
  questionsSources();
  // eslint-disable-next-line no-new
  new LandingBlockSectionH3();
  // eslint-disable-next-line no-new
  new LandingBannerSlider();
  const loadVKApi = () => new Promise((resolve, reject) => {
    if (typeof VK === 'undefined') {
      const script = document.createElement('script');
      script.src = 'https://vk.com/js/api/videoplayer.js';
      script.onload = () => resolve();
      script.onerror = (error) => reject(error);
      document.head.appendChild(script);
    } else {
      resolve();
    }
  });
  loadVKApi().then(() => {
    const landingVideoSlider = new LandingVideo();
    landingVideoSlider.init();
  });
});
