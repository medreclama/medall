import { doctorSelect } from '../../blocks/doctor-select/doctor-select';
import { DoctorCertificates } from '../../blocks/doctor-certificates/doctor-certificates';
import { DoctorWorks } from '../../blocks/doctor-works/doctor-works';
import { HomeFeedback } from '../../blocks/home-feedback/home-feedback';
import ImageCompareClass from '../../common/scripts/modules/image-compare';
import { reviewsItem } from '../../blocks/reviews/reviews-item/reviews-item';

document.addEventListener('DOMContentLoaded', () => {
  doctorSelect.init();
  // eslint-disable-next-line no-new
  new DoctorCertificates();
  // eslint-disable-next-line no-new
  new DoctorWorks();
  // eslint-disable-next-line no-new
  new HomeFeedback();
  const imageCompareOptions = {
    controlColor: '#F5F8F6',
    startingPoint: 10,
  };
  const worksImageCompare = new ImageCompareClass(imageCompareOptions);
  worksImageCompare.init();
  reviewsItem();
});
