import { HomeReasons } from '../../blocks/home-reasons/home-reasons';
import { homeStaffInit } from '../../blocks/home-staff/home-staff';
import { HomeFeedback } from '../../blocks/home-feedback/home-feedback';
import fontResizer from '../../common/scripts/modules/font-resizer';
import { CategoryDirections } from '../../blocks/category-directions/category-directions';
import { categoryDirectionInit } from '../../blocks/category-direction/category-direction';
import counters from '../../common/scripts/modules/counters';
import { reviewsItem } from '../../blocks/reviews/reviews-item/reviews-item';

window.addEventListener('load', () => {
  // eslint-disable-next-line no-new
  new CategoryDirections();
  categoryDirectionInit();
  // eslint-disable-next-line no-new
  new HomeReasons();
  homeStaffInit();
  // eslint-disable-next-line no-new
  new HomeFeedback();
  fontResizer.init('.category-direction__title');
  if (window.location.href === 'https://medall.clinic/dentistry/') {
    counters(`
      <!-- Marquiz script start -->
      <script>
      (function(w, d, s, o){
        var j = d.createElement(s); j.async = true; j.src = '//script.marquiz.ru/v2.js';j.onload = function() {
          if (document.readyState !== 'loading') Marquiz.init(o);
          else document.addEventListener("DOMContentLoaded", function() {
            Marquiz.init(o);
          });
        };
        d.head.insertBefore(j, d.head.firstElementChild);
      })(window, document, 'script', {
          host: '//quiz.marquiz.ru',
          region: 'eu',
          id: '64ee0a16d517b30025efa7ad',
          autoOpen: 15,
          autoOpenFreq: 'once',
          openOnExit: false,
          disableOnMobile: false
        }
      );
      </script>
      <!-- Marquiz script end -->
    `, 'head');
  }
  reviewsItem();
});
