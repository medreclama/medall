import ImageCompareClass from '../../common/scripts/modules/image-compare';
import { WorksFilter } from '../../blocks/works/works-filter/works-filter';

document.addEventListener('DOMContentLoaded', () => {
  const imageCompareOptions = {
    controlColor: '#F5F8F6',
    startingPoint: 10,
  };
  const worksImageCompare = new ImageCompareClass(imageCompareOptions);
  worksImageCompare.init();
  const worksFilter = new WorksFilter('.works-filter');
  worksFilter.init();
});
