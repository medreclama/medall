import { doctorsLayout } from '../../blocks/doctors-layout/doctors-layout';

document.addEventListener('DOMContentLoaded', () => {
  doctorsLayout.init();
});
