class ImageCompareClass {
  constructor(options = {}) {
    this.viewers = document.querySelectorAll('.image-compare');
    this.options = options;
  }

  init() {
    this.viewers.forEach((element) => {
      const imgCompare = new window.ImageCompare(element, this.options);
      imgCompare.mount();
    });

    const observer = new MutationObserver(() => {
      document.body.style.paddingRight = '0px';
    });
    observer.observe(document.body, { attributes: true, attributeFilter: ['style'] });
  }
}

export default ImageCompareClass;
