const resizeElement = (element) => {
  const parent = element.parentNode;
  do element.style.setProperty('font-size', `${parseInt(getComputedStyle(element).fontSize, 10) - 1}px`);
  while (parent.offsetWidth < element.offsetWidth);
};

class FontResizer {
  #elements;
  #resize = () => {
    this.#elements?.forEach((element) => {
      if (element.parentNode.offsetWidth < element.offsetWidth) resizeElement(element);
    });
  };

  init(selector) {
    this.#elements = document.querySelectorAll(selector);
    document.fonts.ready.then(() => this.#resize());
    window.addEventListener('resize', this.#resize);
  }
}

const fontResizer = new FontResizer();

export default fontResizer;
