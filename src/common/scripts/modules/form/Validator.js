export default class Validator {
  constructor($form) {
    this.$form = $form;
    this.$fields = this.getFields();
    this.validationRules = {
      required: 'Обязательное поле',
      email: 'Некорректный e-mail',
    };
    this.$fields.forEach(($field) => {
      $field.addEventListener('change', () => {
        Validator.clearFieldErrorState($field);
        this.validateField($field);
      });
      $field.addEventListener('input', () => {
        Validator.clearFieldErrorState($field);
      });
      $field.addEventListener('focus', () => {
        Validator.clearFieldErrorState($field);
      });
      $field.addEventListener('focusout', () => {
        Validator.clearFieldErrorState($field);
        this.validateField($field);
      });
    });
  }

  getFields() {
    const fields = [];
    for (let i = 0; i < this.$form.elements.length; i += 1) {
      fields.push(this.$form.elements[i]);
    }
    return fields;
  }

  validateFields() {
    this.clearFieldsErrorState();

    let isOk = true;
    this.$fields.forEach(($field) => {
      if (!this.validateField($field)) {
        isOk = false;
      }
    });

    return isOk;
  }

  validateField($field) {
    // eslint-disable-next-line no-restricted-syntax
    for (const rule of Object.keys(this.validationRules)) {
      const Rule = Validator.capitalizeFirstLetter(rule);
      const datasetValidationRule = $field.dataset[`validation${Rule}`];
      if (datasetValidationRule !== undefined) {
        const method = `validate${Rule}`;
        if (!Validator[method]($field)) {
          this.setFieldErrorState($field, datasetValidationRule, rule);
          return false;
        }
      }
    }

    return true;
  }

  setFieldErrorState($field, datasetValidationRuleMessage, rule) {
    $field.classList.add('form-input--error');
    const message = document.createElement('div');
    message.className = 'form-input__error';
    message.innerHTML = datasetValidationRuleMessage || this.validationRules[rule];
    $field.after(message);
  }

  clearFieldsErrorState() {
    this.$fields.forEach(($field) => {
      $field.classList.remove('form-input--error');
    });

    this.$form.querySelectorAll('.form-input__error').forEach(
      ($message) => $message.remove(),
    );
  }

  static clearFieldErrorState($field) {
    $field.classList.remove('form-input--error');
    if ($field.nextElementSibling?.classList.contains('form-input__error')) {
      $field.nextElementSibling.remove();
    }
  }

  static validateRequired($field) {
    let value;
    if ($field.type === 'checkbox') {
      value = $field.checked;
    } else {
      value = $field.value.trim();
    }

    return Boolean(value);
  }

  static validateEmail($field) {
    if (!$field.value) {
      return true;
    }
    return /\S+@\S+\.\S+/.test($field.value);
  }

  static capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
}
