import GLightbox from 'glightbox';

const lb = () => {
  const lightbox = GLightbox({ selector: '[data-glightbox]', width: 600, height: 'fit-content' });

  const lbItems = Array.from(document.querySelectorAll('[data-glightbox]')).filter((item) => !item.dataset.gallery);

  if (lbItems) {
    lbItems.forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        const targetHref = e.currentTarget.getAttribute('href');
        lightbox.setElements([{ href: targetHref }]);
        lightbox.open();
      });
    });
  }
};

export default lb;
