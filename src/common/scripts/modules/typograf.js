/* eslint-disable no-param-reassign */
import Typograf from 'typograf';

export const addTypograf = () => {
  const tp = new Typograf({ locale: ['ru', 'en-US'] });
  tp.disableRule('common/space/trimLeft');

  const processTextNodes = (node) => {
    if (node.nodeType === Node.TEXT_NODE) {
      const parent = node.parentNode;
      if (parent && (parent.classList.contains('footer-partners__name') || parent.classList.contains('works-item__doctor') || parent.classList.contains('home-form__agreement'))) return;

      if (parent.tagName !== 'SCRIPT' && parent.tagName !== 'STYLE') {
        node.textContent = node.textContent.replace(/\sили\s/g, ' или\u00A0');
        node.textContent = node.textContent.replace(/\sтак\s+как\s/g, ' так\u00A0как ');
        node.textContent = node.textContent.replace(/\sгде\s/g, ' где\u00A0');
        node.textContent = node.textContent.replace(/\sпри\s/g, ' при\u00A0');
        node.textContent = node.textContent.replace(/\sдля\s/g, ' для\u00A0');
        node.textContent = tp.execute(node.textContent);
      }
    } else {
      node.childNodes.forEach(processTextNodes);
    }
  };
  processTextNodes(document.body);
};
