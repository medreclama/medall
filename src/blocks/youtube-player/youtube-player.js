/* global YT */

class YoutubePlayer {
  static scriptCreated = false;
  static createScript = () => {
    const script = document.createElement('script');
    script.src = 'https://www.youtube.com/iframe_api';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(script, firstScriptTag);
    YoutubePlayer.scriptCreated = true;

    window.onYouTubeIframeAPIReady = () => window.dispatchEvent(new Event('YTScriptLoaded'));
  };

  #id;
  #states = {
    '-1': 'not started',
    0: 'ended',
    1: 'playing',
    2: 'paused',
    3: 'buffering',
    5: 'queued',
  };

  #events = ['scroll', 'mousemove'];

  play = () => this.player.playVideo();
  pause = () => this.player.pauseVideo();

  get state() {
    if (this.player && typeof this.player.getPlayerState === 'function') {
      return this.#states[`${this.player.getPlayerState()}`];
    }
    return 'not started';
  }

  constructor(id) {
    this.#id = id;
  }

  windowOnMove = () => {
    if (!YoutubePlayer.scriptCreated) YoutubePlayer.createScript();
    this.#events.forEach((event) => window.removeEventListener(event, this.windowOnMove));
  };

  init = () => {
    this.#events.forEach((event) => window.addEventListener(event, this.windowOnMove));
    window.addEventListener('YTScriptLoaded', () => {
      const { videoId } = document.getElementById(this.#id).dataset;
      this.player = new YT.Player(this.#id, {
        height: '360',
        width: '640',
        videoId,
      });
    });
  };
}

export default YoutubePlayer;
