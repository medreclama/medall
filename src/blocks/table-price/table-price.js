/* eslint-disable no-param-reassign */
const tablePrice = document.querySelector('.table-price');
const ELEMENTS_TO_SHOW = 7;
let currentIndex = 0;

const createLoadButton = () => {
  const button = document.createElement('a');
  button.setAttribute('href', '#');
  button.classList.add('table-price__load-more');
  button.textContent = 'Показать еще';
  return button;
};

const updateRowsVisibility = (rows) => {
  rows.forEach((row, index) => {
    row.style.display = index < currentIndex ? '' : 'none';
  });
};

const showRows = (rows, loadMoreButton) => {
  currentIndex += ELEMENTS_TO_SHOW;
  updateRowsVisibility(rows);
  if (currentIndex >= rows.length) loadMoreButton.remove();
};

export const tablePriceInit = () => {
  if (!tablePrice) return;
  const rows = Array.from(tablePrice.querySelectorAll('.table-price__row'));
  const loadMoreButton = createLoadButton();

  currentIndex = ELEMENTS_TO_SHOW;
  updateRowsVisibility(rows);

  if (rows.length > ELEMENTS_TO_SHOW) {
    loadMoreButton.addEventListener('click', (event) => {
      event.preventDefault();
      showRows(rows, loadMoreButton);
    });
    tablePrice.appendChild(loadMoreButton);
  }
};
