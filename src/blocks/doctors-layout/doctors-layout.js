import { removeHashFromUrl, shuffleArray } from '../../common/scripts/modules/helpers';
import headerNavigation from '../header-navigation/header-navigation';

export class DoctorsLayout {
  #element = document.querySelector('.doctors-layout');
  #content = this.#element.querySelector('.content');
  #select = this.#element.querySelector('.doctors-layout__select .form-input__field');
  #categories = this.#element.querySelectorAll('.doctors-layout__category');
  #options = this.#select.options;

  #randomizeItems() {
    this.#categories.forEach((category) => {
      const list = category.querySelector('.doctors-layout__list');
      const items = list.querySelectorAll('.doctors-layout__item');
      const shuffledItems = shuffleArray(Array.from(items));
      Array.from(list.children).forEach((item) => item.remove());
      shuffledItems.forEach((item) => list.append(item));
    });
  }

  #hashHandler = () => {
    const hash = window.location.hash.slice(1);
    const option = Array.from(this.#options).find((opt) => opt.value === hash);
    if (option) {
      const selectInstance = window.select.find((select) => select.element === this.#select.parentNode);
      selectInstance.changeValue({ dataset: { value: option.value } });
    }
  };

  #onHashChange = () => {
    headerNavigation.close();
    this.#hashHandler();
  };

  #onLoad() {
    if (window.location.hash) this.#hashHandler();
    this.#element.classList.add('doctors-layout--loaded');
  }

  #onSelectChange = () => {
    if (this.#select.selectedOptions[0].value !== 'all') {
      const { value } = this.#select.selectedOptions[0];
      window.location.hash = value;
      this.#displaySelected(value);
    } else {
      removeHashFromUrl();
      this.#displayAll();
    }
  };

  #displaySelected = (value) => {
    const currentCategory = Array.from(this.#categories).find((category) => category.dataset.category === value);
    const categories = this.#element.querySelectorAll('.doctors-layout__category');
    categories.forEach((category) => category.remove());
    this.#content.append(currentCategory);
  };

  #displayAll = () => {
    const currentCategory = this.#element.querySelectorAll('.doctors-layout__category');
    currentCategory.forEach((item) => item.remove());
    this.#categories.forEach((category) => this.#content.append(category));
  };

  init() {
    this.#select.addEventListener('change', this.#onSelectChange);
    this.#randomizeItems();
    this.#onLoad();
    window.addEventListener('hashchange', this.#onHashChange);
  }
}

export const doctorsLayout = new DoctorsLayout();
