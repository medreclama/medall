import SimpleBar from 'simplebar';

class CategoryDirection {
  #element;
  #menu;
  #menuName;
  #menuList;
  #menuClass = 'category-direction__menu';
  #menuClassActive = 'form-input--active';
  #menuNameClass = 'category-direction__menu-name';
  #menuListClass = 'category-direction__list';

  constructor(element) {
    this.#element = element;
    this.#menu = this.#element.querySelector(`.${this.#menuClass}`);
    if (this.#menu) {
      this.#menuName = this.#menu.querySelector(`.${this.#menuNameClass}`);
      this.#menuList = this.#menu.querySelector(`.${this.#menuListClass}`);
    }
  }

  open = () => this.#menu.classList.add(this.#menuClassActive);
  close = () => {
    window.removeEventListener('scroll', this.close);
    window.removeEventListener('click', this.#windowClickHandler);
    this.#menu.classList.remove(this.#menuClassActive);
  };

  #windowClickHandler = (event) => {
    if (event.target !== this.#menu) this.close();
  };

  #menuNameClickHandler = () => {
    if (this.#menu.classList.contains(this.#menuClassActive)) this.close();
    else {
      this.open();
      setTimeout(() => {
        window.addEventListener('click', this.#windowClickHandler);
        window.addEventListener('scroll', this.close);
      }, 10);
    }
  };

  init = () => {
    if (!this.#menuName) return;
    this.#menuName.addEventListener('click', this.#menuNameClickHandler);
    // eslint-disable-next-line no-new
    new SimpleBar(this.#menuList);
  };
}

const categoryDirectionElements = document.querySelectorAll('.category-direction');

export const categoryDirectionInit = () => categoryDirectionElements.forEach((element) => {
  const categoryDirection = new CategoryDirection(element);
  categoryDirection.init();
});
