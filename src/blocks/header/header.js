const accessibilityButtonClickHandler = () => {
  const settings = JSON.parse(localStorage.getItem('accessibilityMode')) || {};
  const className = 'accessibility-mode';
  const classes = Object.entries(settings).map((item) => {
    if (item[0] === 'enabled') return false;
    return `${className}--${item[0]}--${item[1]}`;
  });
  classes.unshift(className);
  classes.map((item) => !!item);
  if (document.documentElement.classList.contains(className)) {
    classes.forEach((item) => document.documentElement.classList.remove(item));
    settings.enabled = false;
  } else {
    classes.forEach((item) => document.documentElement.classList.add(item));
    settings.enabled = true;
  }
  localStorage.setItem('accessibilityMode', JSON.stringify(settings));
};

class Header {
  #element;
  #accessibilityButton;
  #viewportHeight = window.innerHeight;
  #lastKnownScrollPosition = 0;
  #signButtons;
  #signForm;

  constructor(element) {
    this.#element = element;
    this.#accessibilityButton = this.#element?.querySelector('.header__accessibility-button');
    this.#signButtons = this.#element?.querySelectorAll('.header__sign-button');
    this.#signForm = document.querySelector('.form:not(.landing-header__form .form):not(#review-form .form)');
  }

  #onScroll = () => {
    if (document.documentElement.scrollTop > 0 && !this.#element.classList.contains('header--fixed')) this.#element.classList.add('header--fixed');
    else if (document.documentElement.scrollTop <= 0) this.#element.classList.remove('header--fixed');
    if (window.scrollY > this.#viewportHeight) this.#element.classList.add('header--fixed-down');
    else this.#element.classList.remove('header--fixed-down');
    if (window.scrollY < this.#lastKnownScrollPosition) this.#element.classList.remove('header--fixed-down');
    this.#lastKnownScrollPosition = window.scrollY;
  };

  #onClick = (evt) => {
    evt.preventDefault();
    this.#signForm.scrollIntoView({ behavior: 'instant' });
  };

  init = () => {
    window.addEventListener('scroll', this.#onScroll);
    this.#signButtons.forEach((button) => {
      if (this.#signForm) {
        // eslint-disable-next-line no-param-reassign
        button.style.display = 'block';
        button.addEventListener('click', this.#onClick);
      // eslint-disable-next-line no-param-reassign
      } else button.style.display = 'none';
    });
    this.#accessibilityButton.addEventListener('click', accessibilityButtonClickHandler);
  };
}

const header = new Header(document.querySelector('.header'));

export default header;
