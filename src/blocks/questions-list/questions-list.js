const questionsList = () => {
  const questionItems = Array.from(document.querySelectorAll('.questions-list__item'));
  if (questionItems.length === 0) return;
  questionItems.forEach((item) => {
    item.addEventListener('click', (event) => {
      if (event.target.closest('.questions-list__name')) event.currentTarget.classList.toggle('questions-list__item--active');
    });
  });
};

const questionsSources = () => {
  const sources = document.querySelector('.questions-list__sources');
  if (!sources) return;
  sources.addEventListener('click', () => {
    sources.classList.toggle('questions-list__sources--active');
  });
};

export { questionsList, questionsSources };
