import {
  Swiper,
  Navigation,
  Pagination,
  Controller,
  FreeMode,
  Autoplay,
  Keyboard,
} from 'swiper';

class Slider {
  #slider;
  #elem;
  #modules = [Navigation, Pagination, Controller, FreeMode, Autoplay, Keyboard];
  #pagination;
  #recievedSettings;
  #paginationSettings = {
    // el: '.slider__pagination',
    lockClass: 'slider__pagination--lock',
    bulletClass: 'slider__pagination-item',
    bulletActiveClass: 'slider__pagination-item--active',
  };

  #settings = {
    modules: this.#modules,
    navigation: {
      nextEl: '.slider__arrow--next',
      prevEl: '.slider__arrow--prev',
      disabledClass: 'slider__arrow--disabled',
      lockClass: 'slider__arrow--lock',
    },
    // on: {
    //   activeIndexChange: (swiper) => {
    //     const nextSlide = swiper.slides[swiper.activeIndex + 1];
    //     const images = nextSlide?.querySelectorAll('img');
    //     images?.forEach((image) => image.removeAttribute('loading'));
    //   },
    //   init: (swiper) => {
    //     const nextSlide = swiper.slides[swiper.activeIndex + 1];
    //     const images = nextSlide?.querySelectorAll('img');
    //     images?.forEach((image) => image.removeAttribute('loading'));
    //   },
    // },
  };

  constructor(elem, settings = {}) {
    this.#elem = elem;
    this.#pagination = this.#elem?.querySelector('.slider__pagination');
    if (this.#pagination) {
      this.#settings.pagination = this.#paginationSettings;
      this.#settings.pagination.el = this.#elem?.querySelector('.slider__pagination');
    }
    this.#recievedSettings = settings;
    this.#settings.navigation.nextEl = this.#elem?.querySelector('.slider__arrow--next');
    this.#settings.navigation.prevEl = this.#elem?.querySelector('.slider__arrow--prev');
    Object.assign(this.#settings, this.#recievedSettings);
  }

  get instance() {
    return this.#slider;
  }

  destroy = (mode = true) => {
    this.#slider?.destroy(mode);
  };

  init() {
    this.#slider = this.#elem ? new Swiper(this.#elem, this.#settings) : undefined;
  }
}

export default Slider;
