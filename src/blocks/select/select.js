import SimpleBar from 'simplebar';

class Select {
  #field;
  #options;
  #optionsList = document.createElement('ul');
  #current = document.createElement('div');
  #activeClass = 'form-input--active';
  #hasValueClass = 'form-input--has-value';
  #simplebar;
  #label;

  #onInput = () => {
    if (this.#field.value !== '' && !this.element.classList.contains(this.#hasValueClass)) {
      this.element.classList.add(this.#hasValueClass);
      this.#label.classList.add('form-input__label--hidden');
    } else if (this.#field.value === '') {
      this.element.classList.remove(this.#hasValueClass);
      this.#label.classList.remove('form-input__label--hidden');
    }
  };

  constructor(element) {
    this.element = element;
    this.#field = this.element.querySelector('.form-input__field');
    this.#label = this.element.querySelector('.form-input__label');
  }

  #clickHandler = (event) => {
    const { target } = event;
    if (this.#field.disabled) return;
    if (target.closest('.form-input__current') && !this.element.classList.contains(this.#activeClass)) this.open();
    else if (target.closest('.form-input__current') && this.element.classList.contains(this.#activeClass)) this.close();
    if (target.closest('.form-input__option')) {
      this.changeValue(target.closest('.form-input__option'));
      this.close();
    }
  };

  #clickWindowHandler = (event) => {
    if (!this.element.contains(event.target) && event.target !== this.element) this.close();
    window.removeEventListener('click', this.#clickWindowHandler);
  };

  #clickButtonHandler = () => {
    if (this.#field.disabled) return;
    if (this.element.classList.contains(this.#activeClass)) this.close();
    else this.open();
  };

  #listChangeHandler = () => {
    this.#optionsList.innerHTML = '';
    this.createOptions();
  };

  createOptions = (searchable = false) => {
    while (this.#simplebar?.getContentElement().firstChild) this.#simplebar?.getContentElement().removeChild(this.#simplebar?.getContentElement().firstChild);
    this.#options = this.element.querySelectorAll('option');
    if (searchable) {
      const searchInputHTML = '<li class="form-input--select-search"><input type="search" class="form-input__field form-input__select-search-field" placeholder="Быстрый поиск"></li>';
      if (this.#simplebar) this.#simplebar.getContentElement().insertAdjacentHTML('beforeend', searchInputHTML);
      else this.#optionsList.insertAdjacentHTML('beforeend', searchInputHTML);
      this.#simplebar.getContentElement().querySelector('.form-input__select-search-field').addEventListener('input', this.#filterOptions);
    }
    this.#options.forEach((option) => {
      const optionHTML = `<li data-value="${option.value}" class="form-input__option">${option.innerText}</li>`;
      if (this.#simplebar) this.#simplebar.getContentElement().insertAdjacentHTML('beforeend', optionHTML);
      else this.#optionsList.insertAdjacentHTML('beforeend', optionHTML);
    });
    this.#simplebar = this.#simplebar || new SimpleBar(this.#optionsList);
  };

  #filterOptions = (event) => {
    const searchTerm = event.target.value.toLowerCase();
    const options = this.#simplebar.getContentElement().querySelectorAll('.form-input__option');
    options.forEach((option) => {
      // eslint-disable-next-line no-param-reassign
      if (option.innerText.toLowerCase().includes(searchTerm)) option.style.display = '';
      // eslint-disable-next-line no-param-reassign
      else option.style.display = 'none';
    });
  };

  #createButton = () => {
    const button = document.createElement('button');
    button.classList.add('form-input__select-button');
    button.type = 'button';
    button.addEventListener('click', this.#clickButtonHandler);
    this.element.append(button);
  };

  open = () => {
    this.element.classList.add(this.#activeClass);
    setTimeout(() => window.addEventListener('click', this.#clickWindowHandler), 30);
  };

  close = () => this.element.classList.remove(this.#activeClass);

  changeValue = (option) => {
    this.#field.value = option.dataset.value;
    const selectedOption = Array.from(this.#field.options).find((opt) => opt.value === this.#field.value);
    if (selectedOption) {
      this.#current.innerText = selectedOption.text;
      const eventChange = new Event('change', { bubbles: true });
      this.#field.dispatchEvent(eventChange);
    }
  };

  init(searchable = false) {
    this.#current.classList.add('form-input__current');
    this.element.append(this.#current);

    this.#optionsList.classList.add('form-input__options');
    this.element.append(this.#optionsList);

    this.createOptions(searchable);

    this.element.addEventListener('click', this.#clickHandler);
    this.#field.addEventListener('change', this.#onInput);
    this.#field.addEventListener('listChange', this.#listChangeHandler);
    this.#createButton();
    window.select = window.select || [];
    window.select.push(this);
  }
}

export default Select;
