import { breakpoints } from '../../common/scripts/enums';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { shuffleArray } from '../../common/scripts/modules/helpers';

export class HomeFeedback {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-feedback .swiper');
  #slider;
  #slides = this.#element?.querySelectorAll('.slide');
  #sliderWrapper = this.#element?.querySelector('.swiper-wrapper');
  #observer;
  #sliderSettings = {
    cssMode: true,
    pagination: {
      el: '.slider__pagination',
      dynamicBullets: true,
    },
    slidesPerView: 1,
    spaceBetween: 40,
    centerInsufficientSlides: true,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 2,
        cssMode: false,
      },
    },
  };

  #randomizeSlides = () => {
    if (!this.#element) return;
    const shuffledSlides = shuffleArray(Array.from(this.#slides));
    Array.from(this.#sliderWrapper.children).forEach((item) => item.remove());
    shuffledSlides.forEach((item) => this.#sliderWrapper.append(item));
  };

  constructor() {
    this.#randomizeSlides();
    this.init();
  }

  init = () => {
    if (this.#element) {
      if (this.#element.parentNode.classList.contains('home-feedback__slider--slides-per-group--1')) delete this.#sliderSettings.breakpoints;
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
    }
  };
}
