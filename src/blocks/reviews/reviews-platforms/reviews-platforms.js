import { breakpoints } from '../../../common/scripts/enums';
import { ClassObserver } from '../../../common/scripts/modules/ClassObserver';

export class ReviewsPlatforms {
  #html = document.querySelector('html');
  #element = document.querySelector('.reviews-platforms .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 40,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 3,
      },
      [breakpoints.m]: {
        slidesPerView: 5,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
    }
  };
}
