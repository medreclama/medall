/* eslint-disable no-param-reassign */
export const reviewsItem = () => {
  const reviewsText = document.querySelectorAll('.reviews-item__text');
  const reviewsContainer = document.querySelector('.reviews-item-list');
  const tagsToRemove = ['br', 'pre'];
  if (!reviewsContainer) return;
  reviewsText.forEach((text) => {
    tagsToRemove.forEach((tag) => {
      text.querySelectorAll(tag).forEach((element) => element.remove());
    });

    const fullText = text.textContent.trim();
    const maxLength = 300;

    if (fullText.length > maxLength) {
      const truncatedText = `${fullText.substring(0, maxLength)}...`;

      text.fullText = fullText;
      text.truncatedText = truncatedText;
      text.innerHTML = truncatedText;

      const toggleButton = document.createElement('a');
      toggleButton.setAttribute('href', '#');
      toggleButton.className = 'reviews-item__button';
      toggleButton.innerText = 'Показать еще';

      text.append(toggleButton);
    }
  });

  reviewsContainer.addEventListener('click', (event) => {
    if (event.target.classList.contains('reviews-item__button')) {
      event.preventDefault();
      const textElement = event.target.closest('.reviews-item__text');

      if (textElement.innerHTML.includes('...')) {
        textElement.innerHTML = textElement.fullText;
        textElement.append(event.target);
        event.target.innerText = 'Скрыть';
      } else {
        textElement.innerHTML = textElement.truncatedText;
        textElement.append(event.target);
        event.target.innerText = 'Показать еще';
      }
    }
  });
};
