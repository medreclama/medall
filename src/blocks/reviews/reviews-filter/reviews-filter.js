/* eslint-disable no-param-reassign */
import { reviewsFilterVideoLoading } from './reviews-filter-video';

export class ReviewsFilter {
  #data;
  #element;
  #serviceSelect;
  #servicesField;
  #doctorSelect;
  #doctorsField;
  #reviewsContainer;
  #reviewsVideoContainer;
  #reviewsItems;
  #reviewsVideoItems;
  #reviewsLoadButton;
  #visibleItemsCount = 6;
  #currentIndex = 0;
  #elementsToShow = 6;
  #filteredItems = [];

  constructor(element) {
    this.#element = document.querySelector(element);
    this.#serviceSelect = this.#element.querySelector('select[name="service"]');
    this.#servicesField = this.#element.querySelector('.reviews-filter__select--services');
    this.#doctorSelect = this.#element.querySelector('select[name="doctor"]');
    this.#doctorsField = this.#element.querySelector('.reviews-filter__select--doctors');
    this.#reviewsContainer = document.querySelector('.reviews-item-list--text');
    this.#reviewsVideoContainer = document.querySelector('.reviews-item-list--video');
    this.#reviewsItems = Array.from(document.querySelectorAll('.reviews-item--text'));
    this.#reviewsVideoItems = Array.from(document.querySelectorAll('.reviews-item--video'));
    this.#reviewsLoadButton = document.querySelector('.reviews-item-load-button');
  }

  async init() {
    this.#data = await ReviewsFilter.fetchData('https://medall.clinic/getDoctors.php');
    this.populateInitialOptions();
    this.#serviceSelect.value = '';
    this.#doctorSelect.value = '';
    this.#tabsInit();
    reviewsFilterVideoLoading();
    this.showInitialItems();
    this.#serviceSelect.addEventListener('change', this.onServiceChange.bind(this));
    this.#doctorSelect.addEventListener('change', this.onDoctorChange.bind(this));
    this.#reviewsLoadButton.addEventListener('click', (evt) => {
      evt.preventDefault();
      this.loadMoreItems();
    });
  }

  #tabsInit() {
    this.#element.addEventListener('click', (evt) => {
      evt.preventDefault();
      if (evt.target.classList.contains('reviews-filter__tabs-video')) {
        this.#reviewsVideoContainer.classList.add('reviews-item-list--video--active');
        evt.target.classList.add('reviews-filter__tabs-video--active');
        this.#element.querySelector('.reviews-filter__tabs-text').classList.remove('reviews-filter__tabs-text--active');
        this.#reviewsContainer.style.display = 'none';
        if (!this.#serviceSelect.value && !this.#doctorSelect.value) this.showInitialItems();
        else this.filterItems();
      } else if (evt.target.classList.contains('reviews-filter__tabs-text')) {
        this.#reviewsVideoContainer.classList.remove('reviews-item-list--video--active');
        evt.target.classList.add('reviews-filter__tabs-text--active');
        this.#element.querySelector('.reviews-filter__tabs-video').classList.remove('reviews-filter__tabs-video--active');
        this.#reviewsContainer.style.display = 'flex';
        if (!this.#serviceSelect.value && !this.#doctorSelect.value) this.showInitialItems();
        else this.filterItems();
      }
    });
  }

  static populateSelect(selectElement, items) {
    selectElement.innerHTML = '';
    items.forEach((item) => {
      const option = document.createElement('option');
      option.value = item.value;
      option.textContent = item.label;
      option.dataset.translit = item.data;
      selectElement.appendChild(option);
    });
  }

  getServices() {
    if (this.#data) {
      return Object.keys(this.#data).flatMap((category) => Object.keys(this.#data[category]).map((service) => ({
        value: service,
        label: service,
      })));
    }
    return [];
  }

  getDoctors() {
    const doctorsMap = {};
    this.#reviewsItems.forEach((el) => {
      const doctorValue = el.getAttribute('data-doctor');
      if (doctorValue) {
        const doctors = doctorValue.split(',').map((name) => name.trim());
        doctors.forEach((doctor) => {
          if (!doctorsMap[doctor]) {
            doctorsMap[doctor] = {
              value: doctor,
              label: doctor,
            };
          }
        });
      }
    });
    return Object.values(doctorsMap);
  }

  getServicesByDoctor(doctor) {
    const uniqueServices = {};
    Object.keys(this.#data).forEach((direction) => {
      Object.keys(this.#data[direction]).forEach((service) => {
        const doctors = this.#data[direction][service];
        if (doctors.some((d) => d.value === doctor)) {
          if (!uniqueServices[service]) {
            uniqueServices[service] = {
              value: service,
              label: service,
            };
          }
        }
      });
    });
    return Object.values(uniqueServices);
  }

  getDoctorsByService(service) {
    const uniqueDoctors = {};
    Object.keys(this.#data).forEach((direction) => {
      if (this.#data[direction][service]) {
        this.#data[direction][service].forEach((doctor) => {
          if (!uniqueDoctors[doctor.value]) {
            uniqueDoctors[doctor.value] = {
              value: doctor.value,
              label: doctor.label,
            };
          }
        });
      }
    });
    return Object.values(uniqueDoctors);
  }

  populateInitialOptions() {
    const services = this.getServices();
    const doctors = this.getDoctors();
    ReviewsFilter.populateSelect(this.#serviceSelect, services);
    window.select.find((select) => select.element === this.#servicesField).createOptions(true);
    ReviewsFilter.populateSelect(this.#doctorSelect, doctors);
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);
  }

  onServiceChange() {
    this.#doctorSelect.value = '';
    const service = this.#serviceSelect.value;
    const doctors = this.getDoctorsByService(service);
    ReviewsFilter.populateSelect(this.#doctorSelect, doctors);
    ReviewsFilter.createClearOption(this.#doctorSelect);
    this.#doctorSelect.value = this.#doctorsField.querySelector('.form-input__current').innerHTML;
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);

    ReviewsFilter.createClearOption(this.#serviceSelect);
    window.select.find((select) => select.element === this.#servicesField).createOptions(true);
    this.clearOptionInit(this.#servicesField);
    this.clearOptionInit(this.#doctorsField);

    this.filterItems();
  }

  onDoctorChange() {
    const doctor = this.#doctorSelect.value;
    const services = this.getServicesByDoctor(doctor);
    ReviewsFilter.populateSelect(this.#serviceSelect, services);
    ReviewsFilter.createClearOption(this.#serviceSelect);
    this.#serviceSelect.value = this.#servicesField.querySelector('.form-input__current').innerHTML;
    window.select.find((select) => select.element === this.#servicesField).createOptions(true);

    ReviewsFilter.createClearOption(this.#doctorSelect);
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);
    this.clearOptionInit(this.#doctorsField);
    this.clearOptionInit(this.#servicesField);

    this.filterItems();
  }

  static createClearOption(selectName) {
    const existingClearOption = Array.from(selectName.options).find((option) => option.value === 'Сбросить фильтр');
    if (existingClearOption) return;
    const clearOption = document.createElement('option');
    clearOption.value = 'Сбросить фильтр';
    clearOption.innerHTML = 'Сбросить фильтр';
    clearOption.dataset.translit = '';
    selectName.appendChild(clearOption);
  }

  resetFilters() {
    this.populateInitialOptions();
    this.#serviceSelect.value = '';
    this.#doctorSelect.value = '';
    ReviewsFilter.clearField(this.#servicesField);
    ReviewsFilter.clearField(this.#doctorsField);
    this.filterItems();
  }

  clearOptionInit(selectField) {
    const servicesOptions = selectField.querySelectorAll('.form-input__option');
    servicesOptions.forEach((option) => {
      if (option.dataset.value === 'Сбросить фильтр') {
        option.classList.add('reviews-filter__clear');
        option.addEventListener('click', () => {
          this.resetFilters();
        });
      }
    });
  }

  static clearField(field) {
    const currentField = field.querySelector('.form-input__current');
    const label = field.querySelector('.form-input__label');
    field.classList.remove('form-input--has-value');
    currentField.innerHTML = '';
    if (label.classList.contains('form-input__label--hidden')) label.classList.remove('form-input__label--hidden');
  }

  filterItems() {
    const serviceValue = this.#serviceSelect.value;
    const doctorValue = this.#doctorSelect.value;

    const isVideoTabActive = this.#reviewsVideoContainer?.classList.contains('reviews-item-list--video--active');
    if (isVideoTabActive) {
      this.#filteredItems = this.#reviewsVideoItems.filter((item) => {
        const servicesList = item.dataset.service ? item.dataset.service.split(',').map((service) => service.trim()) : [];
        const matchService = !serviceValue || servicesList.includes(serviceValue);
        const doctorsList = item.dataset.doctor ? item.dataset.doctor.split(',').map((doctor) => doctor.trim()) : [];
        const matchDoctor = !doctorValue || doctorsList.includes(doctorValue);
        return matchService && matchDoctor;
      });

      this.#reviewsVideoItems.forEach((item) => item.remove());
      this.#currentIndex = 0;
      this.showFilteredItems(this.#reviewsVideoContainer);
    } else {
      this.#filteredItems = this.#reviewsItems.filter((item) => {
        const servicesList = item.dataset.service ? item.dataset.service.split(',').map((service) => service.trim()) : [];
        const matchService = !serviceValue || servicesList.includes(serviceValue);
        const doctorsList = item.dataset.doctor ? item.dataset.doctor.split(',').map((doctor) => doctor.trim()) : [];
        const matchDoctor = !doctorValue || doctorsList.includes(doctorValue);
        return matchService && matchDoctor;
      });

      this.#reviewsItems.forEach((item) => item.remove());
      this.#currentIndex = 0;
      this.showFilteredItems(this.#reviewsContainer);
    }

    this.updateLoadButton();
    let resultMessageVisible = true;
    if (this.#filteredItems.length === 0) {
      this.#reviewsLoadButton.style.display = 'none';
      resultMessageVisible = false;
    }
    this.#createMessage(resultMessageVisible);
  }

  showInitialItems() {
    const isVideoTabActive = this.#reviewsVideoContainer?.classList.contains('reviews-item-list--video--active');
    if (isVideoTabActive) {
      const fragment = document.createDocumentFragment();
      this.#reviewsVideoItems.forEach((item, index) => {
        if (index < this.#visibleItemsCount) fragment.appendChild(item);
        else item.remove();
      });
      this.#reviewsVideoContainer.appendChild(fragment);
    } else {
      const fragment = document.createDocumentFragment();
      this.#reviewsItems.forEach((item, index) => {
        if (index < this.#visibleItemsCount) fragment.appendChild(item);
        else item.remove();
      });
      this.#reviewsContainer.appendChild(fragment);
    }

    this.#currentIndex = this.#visibleItemsCount;
    this.updateLoadButton();
  }

  showFilteredItems(container) {
    const fragment = document.createDocumentFragment();
    const itemsToShow = this.#filteredItems.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => fragment.appendChild(item));
    container.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
  }

  showItems(container, items) {
    const fragment = document.createDocumentFragment();
    const itemsToShow = items.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => fragment.appendChild(item));
    container.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
  }

  loadMoreItems() {
    if (this.#filteredItems.length > 0) {
      if (this.#reviewsVideoContainer?.classList.contains('reviews-item-list--video--active')) this.showFilteredItems(this.#reviewsVideoContainer);
      else this.showFilteredItems(this.#reviewsContainer);
    } else if (this.#reviewsVideoContainer?.classList.contains('reviews-item-list--video--active')) {
      this.showItems(this.#reviewsVideoContainer, this.#reviewsVideoItems);
    } else {
      this.showItems(this.#reviewsContainer, this.#reviewsItems);
    }
    this.updateLoadButton();
  }

  updateLoadButton() {
    let totalItems;

    if (this.#filteredItems.length > 0) {
      totalItems = this.#filteredItems.length;
    } else if (this.#reviewsVideoContainer?.classList.contains('reviews-item-list--video--active')) {
      totalItems = this.#reviewsVideoItems.length;
    } else {
      totalItems = this.#reviewsItems.length;
    }

    if (this.#currentIndex >= totalItems) this.#reviewsLoadButton.style.display = 'none';
    else this.#reviewsLoadButton.style.display = 'flex';
  }

  #createMessage(itemsVisible) {
    const existingMessage = this.#element.querySelector('.reviews-filter__no-results');

    if (itemsVisible) {
      if (existingMessage) existingMessage.remove();
    } else if (!existingMessage) {
      const message = document.createElement('div');
      message.className = 'reviews-filter__no-results';
      message.textContent = 'Ничего не найдено';
      this.#element.appendChild(message);
    }
  }

  static async fetchData(url) {
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      return await response.json();
    } catch (error) {
      return {};
    }
  }
}
