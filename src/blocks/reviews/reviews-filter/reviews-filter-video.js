/* eslint-disable no-param-reassign */
const videoTab = document.querySelector('.reviews-filter__tabs-video');

export const reviewsFilterVideoLoading = () => {
  if (!videoTab) return;
  const reviewsFilterVideo = document.querySelectorAll('.iframe-responsive__iframe iframe');
  videoTab.addEventListener('click', () => {
    reviewsFilterVideo.forEach((frame) => {
      frame.src = `${frame.dataset.videoSrc}`;
    });
  });
};
