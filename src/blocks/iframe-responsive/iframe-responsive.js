/* eslint-disable no-param-reassign */
const youtubeFrames = document.querySelectorAll('.iframe-responsive__iframe iframe');

const youtubeLoadingLazy = () => {
  youtubeFrames.forEach((frame) => {
    if (!frame.dataset.youtube) return;
    if (frame.dataset.youtube.startsWith('https://www.youtube.com') || frame.dataset.youtube.startsWith('https://youtu.be')) {
      frame.closest('.page-section').style.display = 'none';
    }
    const userEvents = () => {
      window.removeEventListener('scroll', userEvents);
      window.removeEventListener('mousemove', userEvents);
      frame.src = `${frame.dataset.youtube}`;
    };
    window.addEventListener('scroll', userEvents);
    window.addEventListener('mousemove', userEvents);
  });
};

export default youtubeLoadingLazy;
