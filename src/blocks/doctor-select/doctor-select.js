export class DoctorSelect {
  #element = document.querySelector('.doctor-select');
  #directionsField = this.#element.querySelector('.doctor-select__input--directions');
  #directionsInput = this.#directionsField.querySelector('.form-input__field');
  #itemsField = this.#element.querySelector('.doctor-select__input--items');
  #itemsInput = this.#itemsField.querySelector('.form-input__field');
  #staff = window.doctorSelectStaff;

  #onDirectionChange = () => {
    const { value } = this.#directionsInput;
    while (this.#itemsInput.options.length > 0) this.#itemsInput.options[0].remove();
    this.#staff[value].forEach((item) => {
      this.#itemsInput.insertAdjacentHTML('beforeend', `<option value="${document.location.origin}${item.value}">${item.name}</option>`);
    });
    if (this.#itemsInput.disabled) window.inputs.find((input) => input.element === this.#itemsField).enable();
    window.select.find((select) => select.element === this.#itemsField).createOptions();
  };

  #onItemChange = () => {
    const { value } = this.#itemsInput;
    window.location.href = value;
  };

  init = () => {
    this.#directionsInput.addEventListener('change', this.#onDirectionChange);
    this.#itemsInput.addEventListener('change', this.#onItemChange);
  };
}

export const doctorSelect = new DoctorSelect();
