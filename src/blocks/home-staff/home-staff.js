import { shuffleArray } from '../../common/scripts/modules/helpers';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

class HomeStaff {
  #element = document.querySelector('.home-staff');
  #select = this.#element?.querySelector('.home-staff__select .form-input__field');
  #sliderElement = this.#element?.querySelector('.slider__slides');
  #slides = this.#sliderElement?.querySelectorAll('.home-staff__slide');
  #sliderWrapper = this.#sliderElement?.querySelector('.swiper-wrapper');
  #categories = {};
  #previousCategory = 'all';
  #slider;
  #html = document.querySelector('html');
  #observer;

  #sliderSettings = {
    cssMode: true,
    pagination: {
      el: '.slider__pagination',
      dynamicBullets: true,
    },
    spaceBetween: 40,
    centerInsufficientSlides: true,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 3,
        cssMode: false,
      },
      [breakpoints.m]: {
        slidesPerView: 4,
        cssMode: false,
      },
    },
  };

  #randomizeSlides = () => {
    const shuffledSlides = shuffleArray(Array.from(this.#slides));
    Array.from(this.#sliderWrapper.children).forEach((item) => item.remove());
    shuffledSlides.forEach((item) => this.#sliderWrapper.append(item));
  };

  #createCategories = () => {
    Array.from(this.#sliderWrapper.children).forEach((item) => {
      if (!this.#categories[item.dataset.category]) this.#categories[item.dataset.category] = [];
      this.#categories[item.dataset.category].push(item);
    });
    this.#categories.all = Array.from(this.#sliderWrapper.children);
  };

  #changeList = (event) => {
    const category = event.target.value;
    if (category !== this.#previousCategory) {
      Array.from(this.#sliderWrapper.children)
        .forEach((item) => item.remove());
      this.#categories[category].forEach((item) => this.#sliderWrapper.append(item));
      this.#slider.instance.update();
      this.#slider.instance.slideTo(0, 10);
      this.#previousCategory = category;
    }
  };

  init = () => {
    if (!this.#element || !this.#sliderElement) return;
    this.#randomizeSlides();
    this.#createCategories();
    this.#slider = new window.Slider(this.#sliderElement, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
    this.#select?.addEventListener('change', this.#changeList);
  };
}

export const homeStaffInit = () => {
  const homeStaff = new HomeStaff();
  homeStaff.init();
};
