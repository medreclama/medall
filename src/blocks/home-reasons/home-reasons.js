import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class HomeReasons {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-reasons .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    cssMode: true,
    spaceBetween: 0,
    breakpoints: {
      [breakpoints.s]: {
        cssMode: false,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
    }
  };
}
