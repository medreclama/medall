import { breakpoints } from '../../common/scripts/enums';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { shuffleArray } from '../../common/scripts/modules/helpers';
import ImageCompareClass from '../../common/scripts/modules/image-compare';

export class LandingWorks {
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-works .swiper');
  #slides = this.#element?.querySelectorAll('.landing-works__slide');
  #sliderWrapper = this.#element?.querySelector('.swiper-wrapper');
  #slider;
  #observer;
  #initialSlides = 8;
  #sliderSettings = {
    slidesPerView: 1,
    noSwiping: true,
    noSwipingClass: 'image-compare',
    pagination: {
      el: '.slider__pagination',
      dynamicBullets: true,
      clickable: true,
    },
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      [breakpoints.m]: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
    },
    on: {
      reachEnd: () => {
        this.#loadMoreItems();
      },
    },
  };

  #loadMoreItems = () => {
    fetch('/data/doctors.json')
      .then((response) => response.json())
      .then((data) => {
        const currentPath = window.location.pathname;
        const filteredData = data.filter((item) => item.doctor.SERVICE_URL === currentPath);
        const itemsToAdd = shuffleArray(filteredData.slice(this.#initialSlides, this.#initialSlides + 3));
        if (itemsToAdd.length > 0) {
          itemsToAdd.forEach((item) => {
            const slideHTML = `
              <div class="swiper-slide landing-works__slide">
                <div class="works-item">
                  <div class="image-compare">
                    <img src="${item.photoBefore}" alt="" loading="lazy" decoding="async" width="720" height="720"/>
                    <img src="${item.photoAfter}" alt="" loading="lazy" decoding="async" width="720" height="720"/>
                  </div>
                  <div class="works-item__service">${item.doctor.SERVICE}</div>
                  <div class="works-item__doctor">
                    <a href="${item.doctor.DETAIL_PAGE_URL}">${item.doctor.NAME}</a>
                    <div>${item.doctor.PREVIEW_TEXT}</div>
                  </div>
                </div>
              </div>`;

            this.#sliderWrapper.insertAdjacentHTML('beforeend', slideHTML);
          });
          this.#initialSlides += 3;
          this.#slider.instance.update();
          const imageCompareOptions = {
            controlColor: '#F5F8F6',
            startingPoint: 10,
          };
          const worksImageCompare = new ImageCompareClass(imageCompareOptions);
          worksImageCompare.init();
        }
      });
  };

  #randomizeSlides = () => {
    if (!this.#element) return;
    const shuffledSlides = shuffleArray(Array.from(this.#slides));
    Array.from(this.#sliderWrapper.children).forEach((item) => item.remove());
    shuffledSlides.forEach((item) => this.#sliderWrapper.append(item));
  };

  constructor() {
    this.#randomizeSlides();
    this.init();
  }

  #createSlider = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };

  init = () => {
    if (!this.#html.classList.contains('accessibility-mode')) this.#createSlider();
    this.#observer = new ClassObserver(
      this.#html,
      'accessibility-mode',
      () => {
        if (this.#slider) {
          this.#slider.destroy(false);
          this.#slider = null;
        }
      },
      () => {
        if (!this.#slider) {
          this.#createSlider();
        }
      },
    );
    this.#observer.init();
  };
}
