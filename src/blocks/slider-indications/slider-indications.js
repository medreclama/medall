import { breakpoints } from '../../common/scripts/enums';

const slider = () => {
  const sliderIndicationsElement = document.querySelector('.slider-indications .slider__slides');

  if (sliderIndicationsElement) {
    const sliderSettings = {
      breakpoints: {
        [breakpoints.s]: {
          slidesPerView: 2,
        },
      },
    };
    const sliderIndications = new window.Slider(sliderIndicationsElement, sliderSettings);
    sliderIndications.init();
  }
};

export default slider;
