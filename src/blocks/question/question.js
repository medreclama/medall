const question = () => {
  const questions = Array.from(document.querySelectorAll('.question'));
  if (questions.length === 0) return;
  questions.forEach((item) => {
    item.addEventListener('click', (event) => {
      if (event.target.closest('.question__name')) event.currentTarget.classList.toggle('question--active');
    });
  });
};

export default question;
