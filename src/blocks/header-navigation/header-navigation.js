class HeaderNavigation {
  #element;
  #parents;
  #media = window.matchMedia('(min-width: 720px)');
  #activeClass = 'header-navigation--active';
  #parentActiveClass = 'header-navigation__item--active';

  constructor(element) {
    this.#element = element;
    this.#parents = this.#element.querySelectorAll('.header-navigation__item--parent');
  }

  open = () => this.#element.classList.add(this.#activeClass);
  close = () => {
    this.#element.classList.remove(this.#activeClass);
    document.documentElement.classList.remove('no-scroll');
  };

  #clickHandler = (event) => {
    const { target } = event;
    if (target.classList.contains('header-navigation__open')) {
      this.#element.classList.toggle(this.#activeClass);
      if (this.#element.classList.contains(this.#activeClass)) document.documentElement.classList.add('no-scroll');
      else document.documentElement.classList.remove('no-scroll');
    }
    if (target.parentNode.classList.contains('header-navigation__item--parent') && !target.closest('.accessibility-mode')) {
      if ((this.#media.matches && !target.parentNode.closest('.header-navigation__menu--services')) || !this.#media.matches) {
        event.preventDefault();
        target.parentNode.classList.toggle(this.#parentActiveClass);
      }
    }
  };

  #mobileOpener() {
    Array.from(this.#parents).forEach((parent) => {
      const link = parent.firstElementChild;
      const opener = document.createElement('div');
      opener.classList.add('header-navigation__opener');
      parent.addEventListener('click', (evt) => {
        if (evt.target === parent || evt.target === link) opener.classList.toggle('header-navigation__opener--active');
      });
      link.after(opener);
    });
  }

  init = () => {
    this.#element.addEventListener('click', this.#clickHandler);
    this.#mobileOpener();
  };
}

const headerNavigation = new HeaderNavigation(document.querySelector('.header-navigation'));

export default headerNavigation;
