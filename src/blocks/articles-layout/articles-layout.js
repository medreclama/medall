export class ArticlesLayout {
  #element = document.querySelector('.articles-layout');
  #select = this.#element?.querySelector('.articles-layout__select');
  #selectOptions = this.#element?.querySelectorAll('.form-input__field option');

  init() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const directionValue = urlParams.get('PROPERTY_DIRECTION_ARTICLES_VALUE');
    if (directionValue) {
      this.#select.classList.add('form-input--has-value');
      this.#selectOptions.forEach((option) => {
        if (option.value === directionValue) {
          // eslint-disable-next-line no-param-reassign
          option.selected = true;
          const currentOption = document.querySelector('.form-input__current');
          currentOption.innerHTML = directionValue;
        }
      });
    }
  }
}

export const articlesLayout = new ArticlesLayout();
