const headerServicesList = document.querySelector('.header-services__list');
const headerServicesItems = headerServicesList.querySelectorAll('.header-services__item');
const mobileServicesList = document.querySelector('.header-navigation__menu--services .header-navigation__list');
const mobileServicesItems = Array.from(mobileServicesList.children).filter((item) => item.classList.contains('header-navigation__item') && !item.classList.contains('header-services__item--sub'));

headerServicesItems.forEach((item, index) => {
  if (index === 0) {
    item.classList.add('header-services__item--service');
  }
});

export const headerServices = () => {
  mobileServicesItems.forEach((serviceItem) => {
    const serviceElement = serviceItem.querySelector('a');
    if (serviceElement) {
      const serviceText = serviceElement.textContent.trim();

      // Создаем новый элемент списка для направления
      const newServiceItem = document.createElement('li');
      newServiceItem.classList.add('header-services__item', 'header-services__item--sub', 'header-services__item--sub-1');
      if (serviceItem.classList.contains('header-navigation__item--parent')) newServiceItem.classList.add('header-services__item--parent');

      // Создаем ссылку для направления
      const newServiceLink = document.createElement('a');
      newServiceLink.href = serviceElement.href;
      newServiceLink.textContent = serviceText;
      newServiceItem.appendChild(newServiceLink);

      // Создаем вложенный список для подкатегорий (например, "Грудь", "Лицо", "Тело")
      const newServiceSubList = document.createElement('ul');
      newServiceSubList.classList.add('header-services__list', 'header-services__list--sub', 'header-services__list--sub-2');

      // Находим все подкатегории для этого направления (например, "Грудь", "Лицо", "Тело")
      const subServices = Array.from(serviceItem.querySelectorAll('.header-navigation__item--sub'));
      subServices.forEach((subCategory) => {
        const subServiceTitle = subCategory.querySelector('span'); // Заголовок подкатегории (например, "Грудь")
        const subServiceList = subCategory.querySelector('.header-navigation__list--sub'); // Список подкатегорий

        if (subServiceTitle && subServiceList) {
          const newSubServiceItem = document.createElement('li');
          newSubServiceItem.classList.add('header-services__item', 'header-services__item--parent', 'header-services__item--sub', 'header-services__item--sub-2');

          // Создаем элемент с названием подкатегории
          const newSubServiceTitle = document.createElement('span');
          newSubServiceTitle.textContent = subServiceTitle.textContent.trim();
          newSubServiceItem.appendChild(newSubServiceTitle);

          // Добавляем вложенный список для подкатегорий 3-го уровня
          const newSubServiceList = document.createElement('ul');
          newSubServiceList.classList.add('header-services__list', 'header-services__list--sub', 'header-services__list--sub-3');

          // Проходим по всем подкатегориям и добавляем их в новый список
          Array.from(subServiceList.children).forEach((subItem) => {
            const link = subItem.querySelector('a');
            if (link) {
              const newSubItem = document.createElement('li');
              newSubItem.classList.add('header-services__item', 'header-services__item--sub', 'header-services__item--sub-3');
              const newSubLink = document.createElement('a');
              newSubLink.href = link.href;
              newSubLink.textContent = link.textContent.trim();
              newSubItem.appendChild(newSubLink);
              newSubServiceList.appendChild(newSubItem);
            }
          });

          newSubServiceItem.appendChild(newSubServiceList);
          newServiceSubList.appendChild(newSubServiceItem);
        }
      });

      // Добавляем вложенный список подкатегорий в родительский элемент
      newServiceItem.appendChild(newServiceSubList);

      // Вставляем новый элемент категории в список
      const newNavList = document.querySelector('.header-services__item--service .header-services__list');
      if (newNavList) {
        newNavList.appendChild(newServiceItem);
      }
    }
  });
};

export const headerServicesOthers = () => {
  // Выбираем исходный список меню и целевой контейнер
  const sourceList = document.querySelector('.header-navigation__menu--others .header-navigation__list');
  const destList = document.querySelector('.header-services__list');

  // Перебираем все элементы списка из исходного меню
  const items = sourceList.children;
  Array.from(items).forEach((item) => {
    const link = item.querySelector('a');
    if (!link) return;

    const newItem = document.createElement('li');
    newItem.classList.add('header-services__item');

    // Проверяем, есть ли у элемента подменю
    const subMenu = item.querySelector('.header-navigation__list--sub');
    if (subMenu) {
      newItem.classList.add('header-services__item--parent');

      const newSubMenu = document.createElement('ul');
      newSubMenu.classList.add('header-services__list', 'header-services__list--sub', 'header-services__list--sub-1');

      const subItems = subMenu.children;
      Array.from(subItems).forEach((subItem) => {
        const newSubItem = document.createElement('li');
        newSubItem.classList.add('header-services__item', 'header-services__item--sub', 'header-services__item--sub-1');

        const subLink = subItem.querySelector('a');
        if (subLink) {
          const newSubLink = document.createElement('a');
          newSubLink.href = subLink.href;
          newSubLink.textContent = subLink.textContent;
          newSubItem.appendChild(newSubLink);
        }

        newSubMenu.appendChild(newSubItem);
      });

      newItem.appendChild(newSubMenu);
    }

    // Добавляем ссылку для основного пункта меню
    const newLink = document.createElement('a');
    newLink.href = link.href;
    newLink.textContent = link.textContent;
    newItem.appendChild(newLink);

    // Добавляем новый элемент в целевое меню
    destList.appendChild(newItem);
  });
};
