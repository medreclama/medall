import { breakpoints } from '../../common/scripts/enums';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class CategoryDirections {
  #html = document.querySelector('html');
  #element = document.querySelector('.category-directions .slider__slides');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 40,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 3,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (!this.#element) return;
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (window.matchMedia('(max-width: 720px)').matches && !this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    // if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
    window.media.addChangeListener('min', 's', () => this.#slider.destroy());
    window.media.addChangeListener('max', 's', () => {
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    });
  };
}
