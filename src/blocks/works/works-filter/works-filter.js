/* eslint-disable no-param-reassign */
import { transliterateText } from '../../../common/scripts/modules/helpers';

export class WorksFilter {
  // #data;
  #element;
  #directionSelect;
  #directionsField;
  #serviceSelect;
  #servicesField;
  #doctorSelect;
  #doctorsField;
  #worksContainer;
  #worksItems;
  #worksLoadButton;
  #visibleItemsCount = 12;
  #currentIndex = 0;
  #elementsToShow = 12;
  #filteredItems = [];

  constructor(element) {
    this.#element = document.querySelector(element);
    this.#directionSelect = this.#element.querySelector('select[name="direction"]');
    this.#directionsField = this.#element.querySelector('.works-filter__select--directions');
    this.#serviceSelect = this.#element.querySelector('select[name="service"]');
    this.#servicesField = this.#element.querySelector('.works-filter__select--services');
    this.#doctorSelect = this.#element.querySelector('select[name="doctor"]');
    this.#doctorsField = this.#element.querySelector('.works-filter__select--doctors');
    this.#worksContainer = document.querySelector('.works-item-list');
    this.#worksItems = Array.from(document.querySelectorAll('.works-item'));
    this.#worksLoadButton = document.querySelector('.works-item-load-button');
  }

  init() {
    // this.#data = await WorksFilter.fetchData('https://medall.clinic/getDoctors.php');
    this.populateInitialOptions();
    this.loadStateFromURL();
    this.#directionSelect.addEventListener('change', this.onDirectionChange.bind(this));
    this.#serviceSelect.addEventListener('change', this.onServiceChange.bind(this));
    this.#doctorSelect.addEventListener('change', this.onDoctorChange.bind(this));
    this.#worksLoadButton.addEventListener('click', (evt) => {
      evt.preventDefault();
      this.loadMoreItems();
    });
  }

  showInitialItems() {
    const fragment = document.createDocumentFragment();
    this.#worksItems.forEach((item, index) => {
      if (index < this.#visibleItemsCount) fragment.appendChild(item);
      else item.remove();
    });
    this.#worksContainer.appendChild(fragment);
    this.#currentIndex = this.#visibleItemsCount;
    this.updateLoadButton();
  }

  showFilteredItems() {
    const fragment = document.createDocumentFragment();
    const itemsToShow = this.#filteredItems.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => fragment.appendChild(item));
    this.#worksContainer.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
  }

  showItems() {
    const fragment = document.createDocumentFragment();
    const itemsToShow = this.#worksItems.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => fragment.appendChild(item));
    this.#worksContainer.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
  }

  loadMoreItems() {
    if (this.#filteredItems.length > 0) {
      this.showFilteredItems();
    } else this.showItems();
    this.updateLoadButton();
  }

  updateLoadButton() {
    const totalItems = this.#filteredItems.length > 0 ? this.#filteredItems.length : this.#worksItems.length;
    if (this.#currentIndex >= totalItems) this.#worksLoadButton.style.display = 'none';
    else this.#worksLoadButton.style.display = 'flex';
  }

  loadStateFromURL() {
    const urlParams = new URLSearchParams(window.location.search);
    const direction = urlParams.get('direction');
    const service = urlParams.get('service');
    const doctor = urlParams.get('doctor');

    if (direction) {
      const directionSelectInstance = window.select.find((select) => select.element === this.#directionsField);
      if (directionSelectInstance) {
        const directionOption = Array.from(directionSelectInstance.element.querySelectorAll('option')).find(
          (opt) => opt.dataset.translit === direction.toLowerCase(),
        );
        directionSelectInstance.changeValue({ dataset: { value: directionOption.value } });
      }
      this.onDirectionChange();
    }

    if (service) {
      const serviceSelectInstance = window.select.find((select) => select.element === this.#servicesField);
      if (serviceSelectInstance) {
        const serviceOption = Array.from(serviceSelectInstance.element.querySelectorAll('option')).find(
          (opt) => opt.dataset.translit === service,
        );
        serviceSelectInstance.changeValue({ dataset: { value: serviceOption.value } });
      }
      this.onServiceChange();
    }

    if (doctor) {
      const doctorSelectInstance = window.select.find((select) => select.element === this.#doctorsField);
      if (doctorSelectInstance) {
        const doctorOption = Array.from(doctorSelectInstance.element.querySelectorAll('option')).find(
          (opt) => opt.dataset.translit === doctor,
        );
        doctorSelectInstance.changeValue({ dataset: { value: doctorOption.value } });
      }
      this.onDoctorChange();
    }

    if (!direction && !service && !doctor) this.showInitialItems();
  }

  getDirections() {
    const uniqueDirections = {};
    this.#worksItems.forEach((el) => {
      const directionValue = el.getAttribute('data-direction');
      if (directionValue && !uniqueDirections[directionValue]) {
        uniqueDirections[directionValue] = {
          value: directionValue,
          label: directionValue,
          data: transliterateText(directionValue),
        };
      }
    });
    return Object.values(uniqueDirections);
  }

  getServices() {
    const servicesMap = {};
    this.#worksItems.forEach((el) => {
      const serviceValue = el.getAttribute('data-service');
      if (serviceValue && !servicesMap[serviceValue]) {
        servicesMap[serviceValue] = {
          value: serviceValue,
          label: serviceValue,
          data: transliterateText(serviceValue),
        };
      }
    });
    return Object.values(servicesMap);
  }

  getDoctors() {
    const doctorsMap = {};
    this.#worksItems.forEach((el) => {
      const doctorValue = el.getAttribute('data-doctor');
      if (doctorValue && !doctorsMap[doctorValue]) {
        const transliteratedName = transliterateText(doctorValue);
        doctorsMap[doctorValue] = {
          value: doctorValue,
          label: doctorValue,
          data: transliteratedName,
        };
      }
    });
    return Object.values(doctorsMap);
  }

  populateInitialOptions() {
    const directions = this.getDirections();
    const services = this.getServices();
    const doctors = this.getDoctors();
    WorksFilter.populateSelect(this.#directionSelect, directions);
    window.select.find((select) => select.element === this.#directionsField).createOptions();
    WorksFilter.populateSelect(this.#serviceSelect, services);
    window.select.find((select) => select.element === this.#servicesField).createOptions(true);
    WorksFilter.populateSelect(this.#doctorSelect, doctors);
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);
  }

  getServicesByDoctor(doctor) {
    const uniqueServices = {};
    this.#worksItems.forEach((el) => {
      const serviceValue = el.getAttribute('data-service');
      const doctorValue = el.getAttribute('data-doctor');

      if (serviceValue && doctorValue === doctor && !uniqueServices[serviceValue]) {
        uniqueServices[serviceValue] = {
          value: serviceValue,
          label: serviceValue,
          data: transliterateText(serviceValue),
        };
      }
    });
    return Object.values(uniqueServices);
  }

  getServicesByDirection(direction) {
    const uniqueServices = {};
    this.#worksItems.forEach((el) => {
      const itemDirection = el.getAttribute('data-direction');
      const serviceValue = el.getAttribute('data-service');
      if (itemDirection === direction && serviceValue && !uniqueServices[serviceValue]) {
        uniqueServices[serviceValue] = {
          value: serviceValue,
          label: serviceValue,
          data: transliterateText(serviceValue),
        };
      }
    });

    return Object.values(uniqueServices);
  }

  getDoctorsByDirection(direction) {
    const uniqueDoctors = {};
    this.#worksItems.forEach((el) => {
      const itemDirection = el.getAttribute('data-direction');
      const doctorValue = el.getAttribute('data-doctor');
      if (itemDirection === direction && doctorValue && !uniqueDoctors[doctorValue]) {
        uniqueDoctors[doctorValue] = {
          value: doctorValue,
          label: doctorValue,
          data: transliterateText(doctorValue),
        };
      }
    });
    return Object.values(uniqueDoctors);
  }

  getDoctorsByService(service) {
    const uniqueDoctors = {};
    this.#worksItems.forEach((el) => {
      const doctorValue = el.getAttribute('data-doctor');
      const serviceValue = el.getAttribute('data-service');
      if (serviceValue === service && doctorValue && !uniqueDoctors[doctorValue]) {
        uniqueDoctors[doctorValue] = {
          value: doctorValue,
          label: doctorValue,
          data: transliterateText(doctorValue),
        };
      }
    });
    return Object.values(uniqueDoctors);
  }

  static createClearOption(selectName) {
    const existingClearOption = Array.from(selectName.options).find((option) => option.value === 'Сбросить фильтр');
    if (existingClearOption) return;
    const clearOption = document.createElement('option');
    clearOption.value = 'Сбросить фильтр';
    clearOption.innerHTML = 'Сбросить фильтр';
    clearOption.dataset.translit = '';
    selectName.appendChild(clearOption);
  }

  resetFilters() {
    WorksFilter.updateURLParams('direction', '');
    WorksFilter.updateURLParams('service', '');
    WorksFilter.updateURLParams('doctor', '');
    this.populateInitialOptions();
    this.#directionSelect.value = '';
    this.#serviceSelect.value = '';
    this.#doctorSelect.value = '';
    this.#clearField(this.#directionsField);
    this.#clearField(this.#servicesField);
    this.#clearField(this.#doctorsField);
    this.filterItems();
  }

  clearOptionInit(selectField) {
    const servicesOptions = selectField.querySelectorAll('.form-input__option');
    servicesOptions.forEach((option) => {
      if (option.dataset.value === 'Сбросить фильтр') {
        option.classList.add('works-filter__clear');
        option.addEventListener('click', () => {
          this.resetFilters();
        });
      }
    });
  }

  onDirectionChange() {
    const direction = this.#directionSelect.value;
    const directionTranslit = this.#directionSelect.selectedOptions[0].dataset.translit;
    WorksFilter.updateURLParams('direction', directionTranslit);

    if (direction) {
      const services = this.getServicesByDirection(direction);
      WorksFilter.populateSelect(this.#serviceSelect, services);
      this.#serviceSelect.value = '';
      window.select.find((select) => select.element === this.#servicesField).createOptions(true);

      const doctors = this.getDoctorsByDirection(direction);
      WorksFilter.populateSelect(this.#doctorSelect, doctors);
      this.#doctorSelect.value = '';
      window.select.find((select) => select.element === this.#doctorsField).createOptions(true);

      WorksFilter.updateURLParams('service', '');
      WorksFilter.updateURLParams('doctor', '');
      this.#clearField(this.#servicesField);
      this.#clearField(this.#doctorsField);
    }
    this.filterItems();
  }

  onServiceChange() {
    this.#doctorSelect.value = '';
    this.#directionSelect.value = '';
    const service = this.#serviceSelect.value;
    let foundDirection = '';
    this.#worksItems.forEach((item) => {
      const itemService = item.getAttribute('data-service');
      const itemDirection = item.getAttribute('data-direction');
      if (itemService === service) {
        foundDirection = itemDirection;
      }
    });
    const serviceTranslit = this.#serviceSelect.selectedOptions[0].dataset.translit;
    if (foundDirection) {
      this.#directionSelect.value = foundDirection;
      this.#directionsField.querySelector('.form-input__current').innerHTML = foundDirection;
      this.#directionsField.querySelector('.form-input__label').innerHTML = '';
      const directionTranslit = transliterateText(foundDirection);
      WorksFilter.updateURLParams('direction', directionTranslit);
    }

    WorksFilter.updateURLParams('service', serviceTranslit);

    const doctors = this.getDoctorsByService(service);
    const selectedDoctor = this.#doctorSelect.value;
    if (selectedDoctor && !doctors.find((doc) => doc.value === selectedDoctor)) {
      this.#doctorSelect.value = '';
    }

    WorksFilter.populateSelect(this.#doctorSelect, doctors);
    WorksFilter.createClearOption(this.#doctorSelect);
    this.#doctorSelect.value = this.#doctorsField.querySelector('.form-input__current').innerHTML;
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);
    this.clearOptionInit(this.#doctorsField);
    if (service) {
      WorksFilter.createClearOption(this.#serviceSelect);
      window.select.find((select) => select.element === this.#servicesField).createOptions(true);
      this.clearOptionInit(this.#servicesField);
    }
    this.filterItems();
  }

  #clearField(field) {
    const currentField = field.querySelector('.form-input__current');
    const label = field.querySelector('.form-input__label');
    field.classList.remove('form-input--has-value');
    currentField.innerHTML = '';
    if (field === this.#directionsField) this.#directionsField.querySelector('.form-input__label').innerHTML = 'Выберите направление';
    if (label.classList.contains('form-input__label--hidden')) label.classList.remove('form-input__label--hidden');
  }

  onDoctorChange() {
    const doctor = this.#doctorSelect.value;
    const doctorTranslit = this.#doctorSelect.selectedOptions[0].dataset.translit;
    this.#directionSelect.value = '';
    WorksFilter.updateURLParams('doctor', doctorTranslit);

    WorksFilter.createClearOption(this.#doctorSelect);
    window.select.find((select) => select.element === this.#doctorsField).createOptions(true);
    this.clearOptionInit(this.#doctorsField);

    if (doctor) {
      const services = this.getServicesByDoctor(doctor);
      WorksFilter.populateSelect(this.#serviceSelect, services);
      WorksFilter.createClearOption(this.#serviceSelect);
      this.#serviceSelect.value = this.#servicesField.querySelector('.form-input__current').innerHTML;
      window.select.find((select) => select.element === this.#servicesField).createOptions(true);
      this.clearOptionInit(this.#servicesField);
    }
    this.filterItems();
  }

  filterItems() {
    const directionValue = this.#directionSelect.value;
    const serviceValue = this.#serviceSelect.value;
    const doctorValue = this.#doctorSelect.value;

    this.#filteredItems = this.#worksItems.filter((item) => {
      const matchDirection = !directionValue || directionValue === item.dataset.direction;
      const matchService = !serviceValue || serviceValue === item.dataset.service;
      const matchDoctor = !doctorValue || doctorValue === item.dataset.doctor;
      return matchDirection && matchService && matchDoctor;
    });

    this.#worksItems.forEach((item) => item.remove());
    this.#currentIndex = 0;
    this.showFilteredItems();
    this.updateLoadButton();
    let resultMessageVisible = true;
    if (this.#filteredItems.length === 0) {
      this.#worksLoadButton.style.display = 'none';
      resultMessageVisible = false;
    }
    this.#createMessage(resultMessageVisible);
  }

  static updateURLParams(param, value) {
    const urlParams = new URLSearchParams(window.location.search);
    if (value) {
      urlParams.set(param, value);
    } else {
      urlParams.delete(param);
    }
    window.history.replaceState(null, '', `${window.location.pathname}?${urlParams.toString()}`);
  }

  #createMessage(itemsVisible) {
    const existingMessage = this.#element.querySelector('.works-filter__no-results');

    if (itemsVisible) {
      if (existingMessage) existingMessage.remove();
    } else if (!existingMessage) {
      const message = document.createElement('div');
      message.className = 'works-filter__no-results';
      message.textContent = 'Ничего не найдено';
      this.#element.appendChild(message);
    }
  }

  // static async fetchData(url) {
  //   try {
  //     const response = await fetch(url);
  //     if (!response.ok) {
  //       throw new Error(`HTTP error! status: ${response.status}`);
  //     }
  //     return await response.json();
  //   } catch (error) {
  //     return {};
  //   }
  // }

  static populateSelect(selectElement, items) {
    selectElement.innerHTML = '';
    items.forEach((item) => {
      const option = document.createElement('option');
      option.value = item.value;
      option.textContent = item.label;
      option.dataset.translit = item.data;
      selectElement.appendChild(option);
    });
  }
}
