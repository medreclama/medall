import IMask from 'imask';

export const iMaskInit = () => {
  const telFields = document.querySelectorAll('input[type="tel"]');
  const maskOptions = {
    mask: '+{7}(000)000-00-00',
  };
  telFields.forEach((field) => {
    const telLabel = field.nextElementSibling;
    // eslint-disable-next-line no-new
    const telMask = new IMask(field, maskOptions);
    field.addEventListener('focus', () => {
      telMask.updateOptions({ lazy: false });
      if (telLabel) telLabel.innerHTML = '';
    }, true);
    field.addEventListener('blur', () => {
      if (telLabel) telLabel.innerHTML = 'Телефон';
      telMask.updateOptions({ lazy: true });
    }, true);
  });
};
