import YoutubePlayer from '../youtube-player/youtube-player';
import IntersectionObserverClass from '../../common/scripts/modules/intersectionObserver';

export class FooterGallery {
  #media = window.matchMedia('(min-width: 780px)');
  #element = document.querySelector('.footer-gallery');
  #imageActiveClass = 'footer-gallery__item--active';
  #images = this.#element?.querySelectorAll('.footer-gallery__item');
  #imageGroups = Array.from(Array(6), () => []);
  #video = this.#element?.querySelector('.footer-gallery__video');
  #player;
  #playerId = 'footer-gallery-video';
  #groupChangeTimes = Array.from(Array(6), () => 0);
  #prevChangeTime = 0;
  #minElapsedTimeForSlider = 2000;
  #minElapsedTimeForGroup = 5000;
  #initializedForDesktop = false;
  #slider = this.#element?.querySelector('.footer-gallery__slider');
  #sliderCloseButton = this.#element?.querySelector('.footer-gallery__slider-close');
  #sliderElement = this.#element?.querySelector('.footer-gallery__slider .swiper');
  #sliderActiveClass = 'footer-gallery__slider--active';
  #sliderSettings = {
    loop: true,
    keyboard: {
      enabled: true,
    },
  };

  #sliderInstance = new window.Slider(this.#sliderElement, this.#sliderSettings);

  #setImageGroupsTimers = () => {
    this.#imageGroups.forEach((group, key) => {
      setInterval(() => {
        const isTimePassed = this.#groupChangeTimes[key] + this.#minElapsedTimeForGroup < Date.now()
          && this.#prevChangeTime + this.#minElapsedTimeForSlider < Date.now();
        const random = this.#prevChangeTime === 0 ? 0.9 : 0.125;
        if (Math.random() > random && isTimePassed) {
          const activeItem = group.find((item) => item.classList.contains(this.#imageActiveClass));
          const index = group.indexOf(activeItem);

          activeItem.classList.remove(this.#imageActiveClass);

          this.#groupChangeTimes[key] = Date.now();
          this.#prevChangeTime = Date.now();

          if (index !== group.length - 1) group[index + 1].classList.add(this.#imageActiveClass);
          else group[0].classList.add(this.#imageActiveClass);
        }
      }, 1000);
    });
  };

  #sliderOpen = (slideId) => {
    const slide = document.getElementById(slideId);
    const index = this.#sliderInstance.instance.slides.indexOf(slide);
    this.#sliderInstance.instance.slideTo(index, 0);
    this.#slider.classList.add(this.#sliderActiveClass);
    if (slide.classList.contains('footer-gallery__slide--video')) this.#player.play();
  };

  #sliderClose = () => {
    this.#slider.classList.remove(this.#sliderActiveClass);
    if (this.#player.state === 'playing') this.#player.pause();
  };

  #sliderClickHandler = (event) => {
    const { target } = event;
    if (target === this.#sliderCloseButton) this.#sliderClose();
  };

  #itemClickHandler = (event) => {
    event.preventDefault();
    const { currentTarget } = event;
    const { href } = currentTarget;
    const slideId = href.slice(href.indexOf('#') + 1);
    this.#sliderOpen(slideId);
  };

  #videoPauseOnSlideChange = () => {
    if (this.#player.state === 'playing') this.#player.pause();
  };

  #initDesktop = () => {
    if (this.#media.matches && !this.#initializedForDesktop) {
      this.#initializedForDesktop = true;
      this.#images.forEach((image, key) => {
        let i = 0;
        while (i <= 5) {
          if (key % 6 === i) {
            this.#imageGroups[i].push(image);
            if (this.#imageGroups[i].length === 1) image.classList.add(this.#imageActiveClass);
          }
          i += 1;
        }
      });
      this.#setImageGroupsTimers();
      this.#images.forEach((image) => {
        image.addEventListener('click', this.#itemClickHandler);
      });
      this.#video.addEventListener('click', this.#itemClickHandler);
      this.#slider.addEventListener('click', this.#sliderClickHandler);
    }
  };

  init = () => {
    if (!this.#element) return;
    this.#initDesktop();
    this.#media.addEventListener('change', this.#initDesktop);
    this.#sliderInstance.init();
    this.#player = new YoutubePlayer(this.#playerId);
    const youtubeIntersectionObserver = new IntersectionObserverClass(this.#element, () => {
      this.#player.init();
    });
    youtubeIntersectionObserver.init();
    this.#sliderInstance.instance.on('slideChange', this.#videoPauseOnSlideChange);
  };
}

export const footerGalleryInit = () => {
  const footerGallery = new FooterGallery();
  footerGallery.init();
};
