import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class LandingComfort {
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-comfort .swiper');
  #slider;
  #observer;

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
    }
  };
}
