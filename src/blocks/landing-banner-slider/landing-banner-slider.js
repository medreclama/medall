import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class LandingBannerSlider {
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-banner-slider .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 0,
  };

  constructor() {
    this.init();
  }

  #createSlider = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };

  init = () => {
    if (!this.#html.classList.contains('accessibility-mode')) this.#createSlider();
    this.#observer = new ClassObserver(
      this.#html,
      'accessibility-mode',
      () => {
        if (this.#slider) {
          this.#slider.destroy(false);
          this.#slider = null;
        }
      },
      () => {
        if (!this.#slider) {
          this.#createSlider();
        }
      },
    );
    this.#observer.init();
  };
}
