class HeaderSearch {
  #element;
  #activeClass = 'header-search--active';

  constructor(element) {
    this.#element = element;
  }

  open = () => {
    this.#element.classList.add(this.#activeClass);
    document.querySelector('.header__tel').classList.add('header__tel--hidden');
    document.querySelector('.header__address').classList.add('header__address--hidden');
  };

  close = () => {
    this.#element.classList.remove(this.#activeClass);
    document.querySelector('.header__tel').classList.remove('header__tel--hidden');
    document.querySelector('.header__address').classList.remove('header__address--hidden');
  };

  #clickHandler = (event) => {
    const { target } = event;
    if (target.classList.contains('header-search__open')) this.open();
    if (target.classList.contains('header-search__close')) this.close();
  };

  init = () => {
    this.#element?.addEventListener('click', this.#clickHandler);
  };
}

const headerSearch = new HeaderSearch(document.querySelector('.header-search'));

export default headerSearch;
