const setHeight = (elem, w) => {
  const width = w || elem.offsetWidth;
  elem.style.setProperty('height', `${width}px`);
};

const landingReason = () => {
  const ri = Array.from(document.querySelectorAll('.landing-reason__image, .landing-reason__add'));
  const rt = Array.from(document.querySelectorAll('.landing-reason__info:only-child'));
  if (ri.length > 0 && rt.length > 0) {
    let h = ri[0].offsetWidth;
    ri.forEach((item) => {
      setHeight(item);
    });
    rt.forEach((item) => {
      setHeight(item, h);
    });
    window.addEventListener('resize', () => {
      ri.forEach((item) => {
        setHeight(item);
      });
      rt.forEach((item) => {
        h = ri[0].offsetWidth;
        setHeight(item, h);
      });
    });
  }
};

export default landingReason;
