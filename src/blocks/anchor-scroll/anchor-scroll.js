const anchorScroll = () => {
  const links = document.querySelectorAll('a[href*="#"]:not([href*="/doctor/#"])');
  links.forEach((link) => {
    link.addEventListener('click', (evt) => {
      if (link.origin !== window.location.origin) {
        return;
      }
      evt.preventDefault();
      const blockID = link.getAttribute('href').replace('#', '');
      if (!blockID) return;
      document.getElementById(`${blockID}`).scrollIntoView({
        block: 'start',
      });
    });
  });
};

export default anchorScroll;
