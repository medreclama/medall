import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class HomeDirections {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-directions .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    cssMode: true,
    slidesPerView: 1,
    spaceBetween: 0,
    // loop: true,
    breakpoints: {
      [breakpoints.s]: {
        cssMode: false,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}
