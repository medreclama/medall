import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class DoctorCertificates {
  #html = document.querySelector('html');
  #element = document.querySelector('.doctor-certificates .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
      [breakpoints.m]: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}
