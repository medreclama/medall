import SimpleBar from 'simplebar';
import Select from '../select/select';

class Field {
  #field;
  #label;

  #onInput = () => {
    if (this.#field?.value !== '' && !this.element.classList.contains('form-input--has-value')) {
      this.element.classList.add('form-input--has-value');
      this.#label?.classList.add('form-input__label--hidden');
    } else if (this.#field?.value === '') {
      this.element.classList.remove('form-input--has-value');
      this.#label?.classList.remove('form-input__label--hidden');
    }
  };

  constructor(element) {
    this.element = element;
    this.#field = this.element.querySelector('.form-input__field');
    this.#label = this.element.querySelector('.form-input__label');
  }

  #passwordFieldProcessing() {
    this.element.insertAdjacentHTML('beforeend', '<button type="button" class="button button--secondary button--narrow form-input__password-toggle"></button>');
    this.element.addEventListener('click', (event) => {
      const { target } = event;
      if (target.classList.contains('form-input__password-toggle')) {
        event.preventDefault();
        if (this.#field.type === 'password') this.#field.type = 'text';
        else this.#field.type = 'password';
      }
    });
  }

  #selectProcessing() {
    const select = new Select(this.element);
    select.init();
  }

  #optionsProcessing = () => {
    const optionsList = this.element.querySelector('.form-input__options');
    const options = this.element.querySelectorAll('.form-input__option');
    // eslint-disable-next-line no-new
    new SimpleBar(optionsList);

    const close = () => this.element.classList.remove('form-input--active');

    const clickWindowHandler = (event) => {
      if (!this.element.contains(event.target) && event.target !== this.element) close();
      window.removeEventListener('click', clickWindowHandler);
    };

    const open = () => {
      this.element.classList.add('form-input--active');
      setTimeout(() => window.addEventListener('click', clickWindowHandler), 30);
    };

    this.element.addEventListener('optionSelected', () => setTimeout(close, 30));

    this.element.addEventListener('click', (event) => {
      const { target } = event;
      if (target.classList.contains('form-input__option')) {
        this.#field.value = target.innerText;
        this.#field.dispatchEvent(new Event('input'));
        this.element.dispatchEvent(new CustomEvent('optionSelected'));
        close();
      } else if (!target.closest('.form-input__select-button') && !this.element.classList.contains('form-input--active')) open();
    });

    const createButton = () => {
      const button = document.createElement('button');
      button.classList.add('form-input__select-button');
      button.type = 'button';
      button.addEventListener('click', () => {
        if (this.element.classList.contains('form-input--active')) close();
        else open();
      });
      this.element.append(button);
    };

    createButton();

    this.#field.addEventListener('input', (event) => {
      const { target } = event;
      if (!this.element.classList.contains('form-input--active')) open();
      options.forEach((option) => {
        const value = option.innerText;
        if (value.toUpperCase().indexOf(target.value.toUpperCase()) > -1) option.style.removeProperty('display');
        else option.style.setProperty('display', 'none');
      });
    });
  };

  #fileProcessing = () => {
    this.#field.addEventListener('input', () => {
      const filename = this.element.querySelector('.form-input__filename') || document.createElement('div');
      filename.classList.add('form-input__filename');
      filename.innerHTML = `${this.#field.value.replace(/^.*[\\/]/, '')}<button class="form-input__clear" type="button"></button>`;
      this.element.append(filename);
      const clear = this.element.querySelector('.form-input__clear');
      clear.addEventListener('click', (event) => {
        event.preventDefault();
        this.#field.value = '';
        filename.remove();
      });
    });
  };

  disable = () => {
    this.element.classList.add('form-input--disabled');
    this.#field.setAttribute('disabled', true);
  };

  enable = () => {
    this.element.classList.remove('form-input--disabled');
    this.#field.removeAttribute('disabled');
  };

  init() {
    this.#field?.addEventListener('input', this.#onInput);
    if (this.#field?.tagName !== 'SELECT') this.#onInput();
    if (this.#field?.type === 'password') this.#passwordFieldProcessing();
    if (this.#field?.type === 'file') this.#fileProcessing();
    if (this.#field?.tagName === 'SELECT') this.#selectProcessing();
    if (this.element.classList.contains('form-input--options')) this.#optionsProcessing();
    window.inputs = window.inputs || [];
    window.inputs.push(this);
  }
}

const initFields = () => {
  const fields = document.querySelectorAll('.form-input');
  fields.forEach((item) => {
    const field = new Field(item);
    field.init();
  });
};

export { Field, initFields };
