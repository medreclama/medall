const setEndWidth = (end) => {
  const endWidth = document.documentElement.clientWidth - end.getBoundingClientRect().right;
  end.parentElement.style.setProperty('--end-width', `${endWidth}px`);
};

const landingAdvantages = () => {
  const ga = document.querySelector('.landing-advantages__item.grid__cell--m--2');
  const end = document.querySelector('.landing-advantages__end');
  if (ga && end) {
    ga.parentElement.style.setProperty('--item-height', `${ga.offsetWidth}px`);
    setEndWidth(end);
    window.addEventListener('resize', () => {
      ga.parentElement.style.setProperty('--item-height', `${ga.offsetWidth}px`);
      setEndWidth(end);
    });
  }
};

export default landingAdvantages;
