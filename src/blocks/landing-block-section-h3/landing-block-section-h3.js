import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class LandingBlockSectionH3 {
  #media = window.matchMedia('(min-width: 780px)');
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-block-section-h3__body .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    cssMode: true,
    slidesPerView: 1,
    spaceBetween: 0,
    breakpoints: {
      [breakpoints.s]: {
        cssMode: false,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
      if (this.#media.matches) this.#slider.destroy();
      this.#media.addEventListener('change', () => {
        if (this.#media.matches) this.#slider.destroy();
        else this.#slider.init();
      });
    }
  };
}
