import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

const landingBlock = () => {
  const images = Array.from(document.querySelectorAll('.landing-block__image'));
  if (images) {
    images.forEach((image) => {
      const img = image.querySelector('img');
      const slider = image.querySelector('.landing-block__slider');
      if (!slider) {
        const resizeLeft = () => img.style.setProperty('--width', `${Math.min(image.offsetWidth + image.getBoundingClientRect().left, 612)}px`);
        const resizeRight = () => img.style.setProperty('--width', `${Math.min(image.offsetWidth + (document.documentElement.clientWidth - image.getBoundingClientRect().right) - 20, 612)}px`);
        if (image === image.parentElement.firstElementChild) resizeLeft();
        if (image === image.parentElement.lastElementChild) resizeRight();
        window.addEventListener('resize', () => {
          if (image === image.parentElement.firstElementChild) requestAnimationFrame(resizeLeft);
          if (image === image.parentElement.lastElementChild) requestAnimationFrame(resizeRight);
        });
      } else {
        const html = document.querySelector('html');
        const landingBlockSlider = new window.Slider(slider.querySelector('.swiper'));
        if (!html.classList.contains('accessibility-mode')) landingBlockSlider.init();
        const observer = new ClassObserver(html, 'accessibility-mode', () => landingBlockSlider.destroy(false), () => landingBlockSlider.init());
        observer.init();
      }
    });
  }
  const landingBlockTops = document.querySelectorAll('.landing-block__top');
  if (landingBlockTops.length === 0) return;
  landingBlockTops.forEach((topElement) => {
    topElement.addEventListener('click', () => {
      const block = topElement.closest('.landing-block');
      block.classList.toggle('landing-block--drop-down--active');
    });
  });
};

const landingBlockSEO = document.querySelector('.landing-block__body');
const landingBlockSEOInit = () => {
  if (!landingBlockSEO.closest('.landing-block')?.querySelector('img')) {
    return;
  }
  let text = landingBlockSEO.innerHTML.trim();
  text = text.replace(/\s+/g, ' ');
  const match = text.match(/^(.*?)(\s*[-–—]\s*это)/i);
  if (match) {
    const title = match[1].trim();
    let content = text.slice(match[0].length).trim();
    content = content.charAt(0).toUpperCase() + content.slice(1);
    const header = document.createElement('div');
    header.className = 'landing-block__header h2';
    header.textContent = `${title} -\u00A0это`;
    landingBlockSEO.innerHTML = content;
    const parent = landingBlockSEO.closest('.grid.grid--justify--center.page-subsection');
    if (parent) parent.before(header);
  }
};

export { landingBlock, landingBlockSEOInit };
