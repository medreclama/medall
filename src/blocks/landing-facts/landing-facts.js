const landingFacts = () => {
  const elements = document.querySelectorAll('.landing-facts');
  elements.forEach((element) => {
    const sliderElement = element.querySelector('.swiper');
    if (sliderElement) {
      const slider = new window.Slider(sliderElement, {
        loop: true,
        allowTouchMove: false,
        autoplay: {
          delay: 5000,
        },
      });
      slider.init();
    }
  });
};

export default landingFacts;
