import { breakpoints } from '../../common/scripts/enums';

const landingTeam = () => {
  const teamBlocks = document.querySelectorAll('.landing-team');
  if (!teamBlocks) return;
  const photosSettings = {
    slidesPerView: 1,
    centerInsufficientSlides: true,
    breakpoints: {
      [breakpoints.s]: { slidesPerView: 3 },
      [breakpoints.m]: { slidesPerView: 4 },
    },
  };
  teamBlocks.forEach((block) => {
    const sliderPhotosElement = block.querySelector('.landing-team__slider .slider__slides');
    const sliderPhotos = new window.Slider(sliderPhotosElement, photosSettings);
    sliderPhotos.init();
  });
};

export default landingTeam;
