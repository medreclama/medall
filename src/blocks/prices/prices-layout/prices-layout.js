/* eslint-disable no-param-reassign */
import { transliterateText } from '../../../common/scripts/modules/helpers';

export class PricesLayout {
  #element = document.querySelector('.prices-layout');
  #tables = this.#element.querySelectorAll('.prices-table');
  #tablesContainer = this.#element.querySelector('.prices-layout__tables');
  #searchInput = this.#element.querySelector('.prices-filters__search input');

  #tableDirections = [...this.#tables].map((table) => ({
    value: table.dataset.direction,
    label: table.dataset.direction,
    data: transliterateText(table.dataset.direction),
  })).filter((item, index, self) => index === self.findIndex((t) => t.value === item.value));

  #tableServices = [...this.#tables].flatMap((table) => [...table.querySelectorAll('.prices-table__title')].map((title) => ({
    value: title.innerHTML.replace(/,([^\s])/g, ', $1').replace(/лор/gi, 'ЛОР'),
    label: title.innerHTML.replace(/,([^\s])/g, ', $1').replace(/лор/gi, 'ЛОР'),
    direction: table.dataset.direction,
  }))).filter((item, index, self) => index === self.findIndex((t) => t.value === item.value));

  #selectDirection = this.#element?.querySelector('.prices-filters__select--direction .form-input__field');
  #selectService = this.#element?.querySelector('.prices-filters__select--service .form-input__field');
  #pricesLoadButton = this.#element?.querySelector('.prices-layout__filters-button');
  #visibleItemsCount = 7;
  #currentIndex = 0;
  #elementsToShow = 7;
  #filteredItems = [];

  constructor() {
    this.init();
  }

  #formatTitles() {
    this.#tables.forEach((table) => {
      const title = table.querySelector('.prices-table__title');
      title.innerHTML = title.innerHTML.replace(/,([^\s])/g, ', $1');
      title.innerHTML = title.innerHTML.replace(/лор/gi, 'ЛОР');
    });
  }

  #showHide() {
    this.#tablesContainer.addEventListener('click', (event) => {
      const title = event.target.closest('.prices-table__title');
      if (title) {
        const item = title.closest('.prices-table');
        item.classList.toggle('prices-table--active');
      }
    });
  }

  #addLoadMoreButtonForRows() {
    document.querySelectorAll('.prices-table').forEach((table) => {
      const rows = Array.from(table.querySelectorAll('.prices-table__row'));
      let currentIndex = this.#visibleItemsCount;

      if (!table.querySelector('.prices-table__load-more') && rows.length > this.#visibleItemsCount) {
        const loadMoreButton = document.createElement('a');
        loadMoreButton.classList.add('prices-table__load-more');
        loadMoreButton.innerHTML = 'Показать еще';
        loadMoreButton.setAttribute('href', '#');

        rows.forEach((row, index) => {
          row.style.display = index < this.#visibleItemsCount ? '' : 'none';
        });

        table.addEventListener('click', (event) => {
          if (event.target.className === 'prices-table__load-more') {
            event.preventDefault();
            this.#showMoreRows(rows, loadMoreButton, currentIndex);
            currentIndex += this.#visibleItemsCount;
          }
        });

        rows[this.#visibleItemsCount - 1].after(loadMoreButton);
      }
    });
  }

  #showMoreRows(rows, loadMoreButton, currentIndex) {
    const newIndex = currentIndex + this.#visibleItemsCount;
    const itemsToShow = rows.slice(currentIndex, newIndex);

    itemsToShow.forEach((row) => {
      row.style.display = '';
    });

    if (newIndex >= rows.length) {
      loadMoreButton.remove();
    } else {
      rows[newIndex - 1].after(loadMoreButton);
    }
  }

  static populateSelect(select, items) {
    select.innerHTML = '';
    items.forEach((item) => {
      const option = document.createElement('option');
      option.value = item.value;
      option.textContent = item.label;
      option.dataset.translit = item.data;
      if (item.direction) option.dataset.direction = item.direction;
      select.appendChild(option);
    });
  }

  #populateDirectionSelect() {
    const filteredDirections = this.#tableDirections.filter((item) => item.value !== 'Косметология'); // Всегда скрываем "Косметология"
    PricesLayout.populateSelect(this.#selectDirection, filteredDirections);
    window.select.find((select) => select.element === this.#selectDirection.closest('.prices-filters__select--direction')).createOptions();
  }

  #populateServiceSelect() {
    const filteredServices = this.#tableServices.filter((item) => item.direction !== 'Косметология'); // Всегда скрываем "Косметология"
    PricesLayout.populateSelect(this.#selectService, filteredServices);
    window.select.find((select) => select.element === this.#selectService.closest('.prices-filters__select--service')).createOptions();
  }

  static clearSelect(select) {
    const currentField = select.closest('.prices-filters__select').querySelector('.form-input__current');
    const label = select.closest('.prices-filters__select').querySelector('.form-input__label');
    select.closest('.prices-filters__select').classList.remove('form-input--has-value');
    currentField.innerHTML = '';
    if (label.classList.contains('form-input__label--hidden')) label.classList.remove('form-input__label--hidden');
  }

  #showInitialItems() {
    const fragment = document.createDocumentFragment();
    this.#tables.forEach((item, index) => {
      if (index < this.#visibleItemsCount) fragment.appendChild(item);
      else item.remove();
    });
    this.#tablesContainer.appendChild(fragment);
    this.#currentIndex = this.#visibleItemsCount;
    this.#updateLoadButton();
  }

  #showFilteredItems() {
    const fragment = document.createDocumentFragment();
    const itemsToShow = this.#filteredItems.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    if (itemsToShow.length === 0) {
      this.#pricesLoadButton.style.display = 'none';
      return;
    }
    itemsToShow.forEach((item) => {
      item.style.display = '';
      fragment.appendChild(item);
    });
    this.#tablesContainer.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
    this.#updateLoadButton();
  }

  #showItems() {
    const fragment = document.createDocumentFragment();
    const itemsToShow = Array.from(this.#tables).slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => {
      item.style.display = '';
      fragment.appendChild(item);
    });
    this.#tablesContainer.appendChild(fragment);
    this.#currentIndex += this.#elementsToShow;
  }

  #loadMoreItems() {
    if (this.#filteredItems.length > 0) {
      this.#showFilteredItems();
    } else this.#showItems();
    this.#updateLoadButton();
  }

  #updateLoadButton() {
    const totalItems = this.#filteredItems.length > 0 ? this.#filteredItems.length : this.#tables.length; // Общее количество
    this.#pricesLoadButton.style.display = totalItems === 0 || this.#currentIndex >= totalItems ? 'none' : '';
  }

  #filterItems() {
    const directionValue = this.#selectDirection.value;
    const serviceValue = this.#selectService.value;
    const searchValue = this.#searchInput.value.toLowerCase();

    this.#filteredItems = Array.from(this.#tables).filter((item) => {
      // Всегда скрываем "Косметология"
      const { direction } = item.dataset;
      if (direction === 'Косметология') {
        item.style.display = 'none';
        return false;
      }

      const title = item.querySelector('.prices-table__title').innerHTML.toLowerCase();
      const cells = item.querySelectorAll('.prices-table__cell');

      let matchSearch = title.includes(searchValue);
      if (!matchSearch) {
        cells.forEach((cell) => {
          if (cell.innerHTML.toLowerCase().includes(searchValue)) {
            matchSearch = true;
          }
        });
      }

      const matchDirection = !directionValue || directionValue === item.dataset.direction;
      const matchService = !serviceValue || [...item.querySelectorAll('.prices-table__title')].some((serviceTitle) => serviceTitle.innerHTML.trim() === serviceValue);

      if (!directionValue && !serviceValue) return matchSearch;
      return matchSearch && matchDirection && matchService;
    });
    Array.from(this.#tables).forEach((item) => {
      item.style.display = 'none';
    });
    this.#currentIndex = 0;
    this.#updateLoadButton();
    this.#showFilteredItems();
    this.#addLoadMoreButtonForRows();
  }

  #resetFilters() {
    PricesLayout.updateURLParams('direction', '');
    this.#populateDirectionSelect();
    this.#populateServiceSelect();
    this.#selectDirection.value = '';
    this.#selectService.value = '';
    PricesLayout.clearSelect(this.#selectDirection);
    PricesLayout.clearSelect(this.#selectService);
    this.#filterItems();
  }

  static createClearOption(select) {
    const existingClearOption = Array.from(select.options).find((option) => option.value === 'Сбросить фильтр');
    if (existingClearOption) return;
    const clearOption = document.createElement('option');
    clearOption.value = 'Сбросить фильтр';
    clearOption.innerHTML = 'Сбросить фильтр';
    clearOption.dataset.translit = '';
    select.appendChild(clearOption);
    window.select.find((s) => s.element === select.closest('.prices-filters__select')).createOptions();
  }

  #clearOptionInit(select) {
    const options = select.closest('.prices-filters__select').querySelectorAll('.form-input__option');
    options.forEach((option) => {
      if (option.dataset.value === 'Сбросить фильтр') {
        option.classList.add('prices-filters__clear');
        option.addEventListener('click', () => {
          this.#resetFilters();
        });
      }
    });
  }

  static updateURLParams(param, value) {
    const urlParams = new URLSearchParams(window.location.search);
    if (value) {
      urlParams.set(param, value);
    } else {
      urlParams.delete(param);
    }
    window.history.replaceState(null, '', `${window.location.pathname}?${urlParams.toString()}`);
  }

  #loadStateFromURL() {
    const urlParams = new URLSearchParams(window.location.search);
    const direction = urlParams.get('direction');

    if (direction) {
      const directionSelectInstance = window.select.find((select) => select.element === this.#selectDirection.closest('.prices-filters__select--direction'));
      if (directionSelectInstance) {
        const directionOption = Array.from(directionSelectInstance.element.querySelectorAll('option')).find(
          (opt) => opt.dataset.translit === direction.toLowerCase(),
        );
        directionSelectInstance.changeValue({ dataset: { value: directionOption.value } });
      }
      this.#onDirectionChange();
    }

    if (!direction) this.#showInitialItems();
  }

  #onDirectionChange() {
    const selectedDirection = this.#selectDirection.value;
    const directionTranslit = this.#selectDirection.selectedOptions[0].dataset.translit;
    PricesLayout.updateURLParams('direction', directionTranslit);

    if (selectedDirection) {
      const filteredServices = this.#tableServices.filter((service) => service.direction === selectedDirection);
      PricesLayout.populateSelect(this.#selectService, filteredServices);

      window.select.find((select) => select.element === this.#selectService.closest('.prices-filters__select--service')).createOptions();
      PricesLayout.createClearOption(this.#selectDirection);
      PricesLayout.createClearOption(this.#selectService);

      this.#clearOptionInit(this.#selectDirection);
      this.#clearOptionInit(this.#selectService);
      this.#selectService.value = '';
      PricesLayout.clearSelect(this.#selectService);
    }
    this.#filterItems();
  }

  #onServiceChange() {
    const selectedService = this.#selectService.value;
    const service = this.#tableServices.find((s) => s.value === selectedService);
    if (service) {
      const filteredDirections = this.#tableDirections.filter((direction) => direction.value === service.direction);
      PricesLayout.populateSelect(this.#selectDirection, filteredDirections);
      PricesLayout.createClearOption(this.#selectService);
      PricesLayout.createClearOption(this.#selectDirection);

      this.#clearOptionInit(this.#selectService);
      this.#clearOptionInit(this.#selectDirection);
    }
    this.#filterItems();
  }

  #onSearchInputChange() {
    this.#filterItems();
  }

  #hideDuplicates() {
    this.#tables.forEach((table) => {
      const rowTitles = Array.from(table.querySelectorAll('.prices-table__row .prices-table__cell:first-child'));
      const uniqueTitles = new Set();
      rowTitles.forEach((title) => {
        const text = title.innerHTML.trim();
        if (uniqueTitles.has(text)) {
          const row = title.closest('.prices-table__row');
          row.remove();
        } else uniqueTitles.add(text);
      });
    });
  }

  init() {
    this.#formatTitles();
    this.#hideDuplicates();
    this.#showInitialItems();
    this.#populateDirectionSelect();
    this.#populateServiceSelect();

    const directionSelectInstance = window.select.find((select) => select.element === this.#selectDirection.closest('.prices-filters__select--direction'));
    if (directionSelectInstance) directionSelectInstance.changeValue({ dataset: { value: 'Пластическая хирургия' } });
    this.#loadStateFromURL();

    this.#selectService.value = '';
    this.#filterItems();
    this.#onDirectionChange();

    this.#selectDirection.addEventListener('change', this.#onDirectionChange.bind(this));
    this.#selectService.addEventListener('change', this.#onServiceChange.bind(this));
    this.#pricesLoadButton.addEventListener('click', (evt) => {
      evt.preventDefault();
      this.#loadMoreItems();
      this.#addLoadMoreButtonForRows();
    });
    this.#searchInput.addEventListener('input', this.#onSearchInputChange.bind(this));
    // this.#addLoadMoreButtonForRows();
    this.#showHide();
  }
}
