/* global VK */
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class LandingVideo {
  #media = window.matchMedia('(max-width: 780px)');
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-video .swiper');
  #videoElements = this.#element?.querySelectorAll('.landing-video__item');
  #videoElementsIds = this.#element?.querySelectorAll('[data-youtube]');
  #showMoreButton = document.querySelector('.landing-video__button');
  #slider;
  #observer;
  #players = {};
  #playingStates = {};
  #currentPlayingIndex = null;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 40,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 2,
      },
      [breakpoints.m]: {
        slidesPerView: 3,
      },
    },
    on: {
      slideChange: () => {
        if (this.#currentPlayingIndex !== null && this.#players[this.#currentPlayingIndex]) {
          this.#players[this.#currentPlayingIndex].pause();
          this.#playingStates[this.#currentPlayingIndex] = false;
          this.#currentPlayingIndex = null;
        }
      },
    },
  };

  #handleVideoStart = (index) => {
    Object.keys(this.#players).forEach((key) => {
      if (parseInt(key, 10) !== index && this.#players[key]) {
        this.#players[key].pause();
        this.#playingStates[key] = false;
      }
    });
    this.#currentPlayingIndex = index;
    this.#playingStates[index] = true;
  };

  #addVideoHandle = () => {
    this.#videoElementsIds.forEach((video, index) => {
      if (!this.#players[index]) {
        this.#players[index] = VK.VideoPlayer(video);
        this.#players[index].on('started', () => this.#handleVideoStart(index));
        this.#players[index].on('resumed', () => this.#handleVideoStart(index));
        this.#players[index].on('paused', () => {
          this.#playingStates[index] = false;
        });
      }
      this.#playingStates[index] = true;
      this.#currentPlayingIndex = index;
    });
  };

  #userEvents = () => {
    window.removeEventListener('scroll', this.#userEvents);
    window.removeEventListener('mousemove', this.#userEvents);
    this.#videoElementsIds.forEach((frame) => {
      // eslint-disable-next-line no-param-reassign
      frame.src = frame.dataset.youtube;
    });

    this.#addVideoHandle();
  };

  #showNextVideo = () => {
    const hiddenVideos = Array.from(this.#videoElements).filter((video) => video.closest('.landing-video__slide').style.display === 'none');
    if (hiddenVideos.length > 0) {
      const nextVideo = hiddenVideos[0];
      nextVideo.closest('.landing-video__slide').style.display = 'block';
    }
    if (hiddenVideos.length <= 1) this.#showMoreButton.style.display = 'none';
  };

  #hideInitialVideoOnMobile = () => {
    this.#videoElements.forEach((video, index) => {
      // eslint-disable-next-line no-param-reassign
      if (index !== 0) video.closest('.landing-video__slide').style.display = 'none';
    });
  };

  init = () => {
    if (!this.#element) return;

    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();

    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();

    if (this.#media.matches) {
      this.#slider.destroy();
      this.#hideInitialVideoOnMobile();
    }
    this.#media.addEventListener('change', () => {
      if (this.#media.matches) {
        this.#slider.destroy();
        this.#hideInitialVideoOnMobile();
      } else this.#slider.init();
    });

    if (this.#showMoreButton) this.#showMoreButton.addEventListener('click', this.#showNextVideo);

    window.addEventListener('scroll', this.#userEvents);
    window.addEventListener('mousemove', this.#userEvents);
  };
}
