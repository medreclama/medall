import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class LandingStories {
  #html = document.querySelector('html');
  #element = document.querySelector('.landing-stories .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    centeredSlides: true,
    loop: true,
    slidesPerView: 'auto',
  };

  constructor() {
    this.init();
  }

  #createSlider = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };

  init = () => {
    if (!this.#html.classList.contains('accessibility-mode')) this.#createSlider();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => {
      if (this.#slider) this.#slider.init();
      else this.#createSlider();
    });
    this.#observer.init();
  };
}
