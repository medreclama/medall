import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';
import { shuffleArray } from '../../common/scripts/modules/helpers';

export class DoctorWorks {
  #html = document.querySelector('html');
  #element = document.querySelector('.doctor-works .swiper');
  #slides = this.#element?.querySelectorAll('.doctor-works__slide');
  #sliderWrapper = this.#element?.querySelector('.swiper-wrapper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    allowTouchMove: false,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      [breakpoints.m]: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
    },
  };

  #randomizeSlides = () => {
    if (!this.#element) return;
    const shuffledSlides = shuffleArray(Array.from(this.#slides));
    Array.from(this.#sliderWrapper.children).forEach((item) => item.remove());
    shuffledSlides.forEach((item) => this.#sliderWrapper.append(item));
  };

  constructor() {
    this.#randomizeSlides();
    this.init();
  }

  init = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
      this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
      this.#observer.init();
    }
  };
}
