class AccessibilityMode {
  #element = document.documentElement;
  #settings = JSON.parse(localStorage.getItem('accessibilityMode')) || {};
  #className = 'accessibility-mode';

  #setAccessibilityModeOnLoad = () => {
    if (this.#settings.enabled) {
      this.#element.classList.add(this.#className);
      Object.entries(this.#settings).forEach((item) => {
        if (item[0] === 'enabled') return;
        if (item[1] === true) this.#element.classList.add(`${this.#className}--${item[0]}`);
        else this.#element.classList.add(`${this.#className}--${item[0]}--${item[1]}`);
      });
    }
  };

  init = () => {
    this.#setAccessibilityModeOnLoad();
  };
}

export const accessibilityMode = new AccessibilityMode();
