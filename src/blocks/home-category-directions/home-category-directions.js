import { breakpoints } from '../../common/scripts/enums';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class HomeCategoryDirections {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-category-directions .slider__slides');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    spaceBetween: 40,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 2,
      },
      [breakpoints.m]: {
        slidesPerView: 3,
      },
    },
  };

  constructor() {
    this.init();
  }

  #createSlider = () => {
    if (this.#element) {
      this.#slider = new window.Slider(this.#element, this.#sliderSettings);
      this.#slider.init();
    }
  };

  init = () => {
    if (!this.#html.classList.contains('accessibility-mode')) this.#createSlider();
    this.#observer = new ClassObserver(
      this.#html,
      'accessibility-mode',
      () => {
        if (this.#slider) {
          this.#slider.destroy(false);
          this.#slider = null;
        }
      },
      () => {
        if (!this.#slider) {
          this.#createSlider();
        }
      },
    );
    this.#observer.init();
  };
}
