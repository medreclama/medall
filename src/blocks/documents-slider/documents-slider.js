import { breakpoints } from '../../common/scripts/enums';
import { ClassObserver } from '../../common/scripts/modules/ClassObserver';

export class HomeFeedback {
  #html = document.querySelector('html');
  #element = document.querySelector('.documents-slider .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 2,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 4,
        spaceBetween: 40,
      },
      [breakpoints.m]: {
        slidesPerView: 6,
        spaceBetween: 40,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
  };
}
