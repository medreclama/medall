const discountItemsDate = document.querySelectorAll('.discounts-item__date');
export const discountItem = () => {
  discountItemsDate.forEach((date) => {
    const currentDate = new Date();
    const nextMonth = currentDate.getMonth() + 1;
    const lastDay = new Date(currentDate.getFullYear(), nextMonth, 0);
    const lastDayFormatted = `${lastDay.getDate().toString().padStart(2, '0')}.${(lastDay.getMonth() + 1).toString().padStart(2, '0')}.${lastDay.getFullYear()} г.`;
    // eslint-disable-next-line no-param-reassign
    date.textContent = `Акция действует до ${lastDayFormatted}`;
  });
};
