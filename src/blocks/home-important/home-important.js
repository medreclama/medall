import { ClassObserver } from '../../common/scripts/modules/ClassObserver';
import { breakpoints } from '../../common/scripts/enums';

export class HomeImportant {
  #html = document.querySelector('html');
  #element = document.querySelector('.home-important .swiper');
  #slider;
  #observer;
  #sliderSettings = {
    slidesPerView: 1,
    cssMode: true,
    spaceBetween: 20,
    breakpoints: {
      [breakpoints.s]: {
        slidesPerView: 4,
        spaceBetween: 40,
        cssMode: false,
      },
    },
  };

  constructor() {
    this.init();
  }

  init = () => {
    if (!this.#element) return;
    this.#slider = new window.Slider(this.#element, this.#sliderSettings);
    if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    this.#observer = new ClassObserver(this.#html, 'accessibility-mode', () => this.#slider.destroy(false), () => this.#slider.init());
    this.#observer.init();
    window.media.addChangeListener('min', 's', () => this.#slider.destroy());
    window.media.addChangeListener('max', 's', () => {
      if (!this.#html.classList.contains('accessibility-mode')) this.#slider.init();
    });
  };
}
