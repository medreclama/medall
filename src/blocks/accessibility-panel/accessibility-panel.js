class AccessibilityPanel {
  #element;
  #radios;
  #settings;
  #button;
  #html = document.documentElement;
  #className = 'accessibility-mode';

  constructor(element) {
    this.#element = element;
    this.#radios = this.#element?.querySelectorAll('.accessibility-panel__radio');
    this.#button = this.#element?.querySelector('.accessibility-panel__button');
    this.#settings = JSON.parse(localStorage.getItem('accessibilityMode')) || {};
  }

  #radioChangeHandler = ({ target }) => {
    const name = target.name.slice('accessibility-'.length);
    this.#settings[name] = target.value;
    const current = Array.from(this.#html.classList).find((className) => className.indexOf(name) !== -1);
    this.#html.classList.remove(current);
    this.#html.classList.add(`${this.#className}--${name}--${target.value}`);
    localStorage.setItem('accessibilityMode', JSON.stringify(this.#settings));
  };

  #setRadioOnLoad = () => {
    this.#radios.forEach((radio) => {
      radio.setAttribute('checked', false);
    });
    Object.entries(this.#settings).forEach((item) => {
      if (item[0] === 'enabled') return;
      const toEnable = Array.from(this.#radios).find((radio) => radio.name.slice('accessibility-'.length) === item[0] && radio.value === item[1]);
      toEnable.checked = true;
    });
  };

  #buttonClickHandler = () => {
    this.#html.classList.remove(this.#className);
    Object.entries(this.#settings).forEach((item) => {
      if (item[0] === 'enabled') return;
      const toRemove = `${this.#className}--${item[0]}--${item[1]}`;
      this.#html.classList.remove(toRemove);
    });
    this.#settings.enabled = false;
    localStorage.setItem('accessibilityMode', JSON.stringify(this.#settings));
  };

  init = () => {
    if (!this.#element) return;
    this.#setRadioOnLoad();
    this.#button.addEventListener('click', this.#buttonClickHandler);
    this.#radios.forEach((radio) => {
      radio.addEventListener('change', this.#radioChangeHandler);
    });
  };
}

export const accessibilityPanel = new AccessibilityPanel(document.querySelector('.accessibility-panel'));
