export const congressSliderInteresting = () => {
  const sliderIndicationsElement = document.querySelector('.slider-interesting .slider__slides');

  if (sliderIndicationsElement) {
    const sliderSettings = {
    };
    const sliderIndications = new window.Slider(sliderIndicationsElement, sliderSettings);
    sliderIndications.init();
  }
};
