import { breakpoints } from '../../../common/scripts/enums';

export const congressSliderSpeakers = () => {
  const element = document.querySelector('.slider-speakers .slider__slides');

  if (element) {
    const settings = {
      breakpoints: {
        [breakpoints.s]: {
          slidesPerView: 2,
          slidesPerGroup: 2,
          spaceBetween: 40,
        },
      },
    };
    const slider = new window.Slider(element, settings);
    slider.init();
  }
};
