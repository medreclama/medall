export const sliderProgram = () => {
  const element = document.querySelector('.slider-program .slider__slides');

  if (element) {
    const settings = {};
    const slider = new window.Slider(element, settings);
    slider.init();
  }
};
