import gulp from 'gulp';
import svgSprite from 'gulp-svg-sprite';
import { templateDir } from '../gulpfile.babel';
import { readdirSync } from 'fs';

const getDirectories = (source) => {
  let res = [];
  try {
    res = readdirSync(source, { withFileTypes: true })
      .filter(dirent => dirent.isDirectory())
      .map(dirent => dirent.name)
  } catch (err) { console.log('No SVG Sprites found'); }
  return res;
}

const svgDirs = getDirectories('./src/svg-sprites/');

gulp.task('svgSprites',(done) => {
  const getConfig = (name) => {
    return {
      mode: {
        stack: {
          dest: 'images',
          sprite: `${name}.svg`,
        },
      },
    };
  };

  const pipeline = (path, config) => gulp.src(path)
    .pipe(svgSprite(config))
    .pipe(gulp.dest(templateDir));

  svgDirs.forEach((dir) => pipeline(`src/svg-sprites/${dir}/*.svg`, getConfig(dir)));
  pipeline(`src/svg-sprites/*.svg`, getConfig('common'));

  done();
});
