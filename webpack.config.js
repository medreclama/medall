const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'pages';
const mode = isProduction ? 'production' : 'development';

module.exports = {
  mode,
  entry: {
    script: './src/pages/index/script.js',
    main: './src/pages/main/main.js',
    landing: './src/pages/landing/landing.js',
    discounts: './src/pages/discounts/discounts.js',
    category: './src/pages/category/category.js',
    404: './src/pages/404/404.js',
    search: './src/pages/search/search.js',
    doctors: './src/pages/doctors/doctors.js',
    doctor: './src/pages/doctor/doctor.js',
    documents: './src/pages/documents/documents.js',
    contacts: './src/pages/contacts/contacts.js',
    vacancies: './src/pages/vacancies/vacancies.js',
    congress: './src/pages/congress/congress.js',
    works: './src/pages/works/works.js',
    article: './src/pages/article/article.js',
    reviews: './src/pages/reviews/reviews.js',
    prices: './src/pages/prices/prices.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].js',
  },
  devtool: isProduction ? undefined : 'source-map',
  plugins: [new ESLintPlugin()],
  optimization: {
    minimize: isProduction,
  },
};
